# Compiling

You'll need meson to be installed from the [archives](https://github.com/mesonbuild/meson/releases) or pip, etc.

Dependencies are pulled in by meson; if they're installed on the OS then those will be used, otherwise it will download them from source and build them. You can look into the subprojects folder to see which dependencies are being used, and the *.wrap files specify what/how/where it is downloading.

## Release build

1. `meson setup build_release --buildtype release`
1. `meson compile -C build_release`
    1. If you use vscode, you can use the debug functionality to compile/launch for this step as well.
1. run with `./build_release/fpsgame_client -qconfig`

## Debug Build

1. `meson setup build_debug --buildtype debugoptimized`
1. `meson compile -C build_debug`
    1. If you use vscode, you can use the debug functionality to compile/launch for this step as well.
1. run with `./build_debug/fpsgame_client -qconfig`