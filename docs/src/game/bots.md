# Bots

Bots are supported in all multiplayer modes, so long as they have waypoints available to guide them. Bots range in skill from 0 to 100, with an extra skill level 101 making the bots as skilled as possible. You must either be master or playing locally to add or remove bots. At the end of a match, all bots are removed.

### `/addbot (SKILL)`
Adds a bot at skill level SKILL if provided, or defaults to a random skill level between 50 and 100 if not provided. You must be master or playing locally to use this command. Bots may only be added up to the server specified bot limit unless you are an admin or local player. This also causes waypoints to load if any are available for the current map.

### `/delbot`
Removes a bot. You must be master or playing locally to use this command.

### `/botlimit [N]`
Sets the bot limit for masters to N. This limit does not effect admins or local players. Only admins or local players can use this command.

### `/botbalance [B]`
Enables automatic team balancing for bots if B=1 and disables it if B=0. Only masters or local players can use this command. 