# Capture

This is the most complicated multiplayer mode so some explanation is required. This mode is centered around the idea of capturing and holding bases, and has similarities to the game "battlefield 1942" and the "onslaught" mode of unreal 2004: "bases" are placed thruout the map

A base (flag) can be in 2 states: captured or neutral. When captured, it is captured by a certain team. This is shown by the texture its rendered with (red means enemy captured, blue means your team, grey means neutral). When a base is captured, it will produce ammo periodically (every 20 seconds). Your team may take ammo (shown as ammo boxes orbiting the base) by either touching the base, or explicitly with the R key ("repammo" command).

A neutral base can be converted to your team by standing next to it for N seconds (N = 7 or so) within a certain radius. Multiple players speed up the process proportionally. The time accumulated to N is counted by the server as soon as the first player enters the radius, and reset to 0 immediately once all players of the initial team leave the radius.

A base of the enemy color can be converted to neutral in exactly the same way. (so often it is a 2 step process to take over a base). However, it takes twice as long to convert an enemy base to neutral, as it does to convert a neutral base to your team.

Current team name and capture status is shown above the base using a text particle.

At the spawn of a map, all bases are neutral, and all players spawn at normal player spawns, and try to capture whatever they can. Once bases are captured, players will spawn at spawnpoints close to whichever of their bases is closest to an enemy base. spawning will be preferred at bases which are not currently being captured by an enemy.

If a player dies, they wait 5 seconds before they can respawn again. If you have no bases left (the enemy has captured yours, or at least made em neutral) you will spawn at random spawnpoints.

If all bases are captured by one team, then that team wins the game automaticaly. Alternatively, if this doesn't happen, at the timelimit the team that wins is the team that has held the most bases for the most amount of time (1 point per X seconds for every base in your color).

No points are awarded for kills or self-deaths. Infact, no individual score is ever displayed, your score is the same as the team's score.

When you spawn, you are given a certain amount of ammo for randomly 2 weapons out of the 5 main ones, + pistol ammo. Ammo will periodically spawn at bases owned by a given team which can be collected by only that team.

A simple HUD shows bases relative to your current position in red or blue (can be a sphere with blips on it, with blips on the border for faraway bases). 