# Singleplayer

There are two single player modes:

* (mode -3, command "/sp <map>"): a classical progress-based monsters-wake-up-when-they-see-you mode which is very similar to Doom & Quake, and is played on maps specifically designed for SP. Items don't respawn.
* (mode -2, command "/dmsp <map>"): an arena style SP mode (akin to my "DMSP" mod for quake) which you play on DM maps: 10 seconds after the game starts monsters start spawning in (currently about 1 per second), which will immediately be angry at you. The map ends when they all have spawned and they are all killed (either by you or by their buddies ;). The number of monsters depends on the "skill" variable. Items respawn as in DM.

Either mode can be turned into "slow motion SP" by toggling the "slowmosp" var. These modes that work the same as regular SP, except that the gamespeed is directly derived from your health, so the more damage you take, the more in slow motion the game plays. Slow motion, while in one way a punishment for your failures, also makes it easy to escape enemy fire and land perfect hits, therefore allowing you to recover. Besided that, it just looks/feels plain cool. These modes do not support the concept of death, you are simply going "infinitely" slow at health = 1. To compensate, there is health regeneration, which is faster the less health you have. Your score (see SP respawning below) will be measured in real time, so any slow motion time negatively affects your score ("simulation time" shown is the time you would have taken with no slowdown at all).

The mode numbers can most conveniently be set over the singleplayer menu. In both cases, the map ends when all monsters are killed, or an end of level trigger is hit if one is available. Coop play is not supported yet.

There are currently 8 monsters, with the following properties (sorted roughly in order of "threat" they pose):

| model | gun used | damage | stamina | speed | freq | lag | rate | loyalty |
| -------- | ----- | -----: | ------: | ----: | ---: | --: | ---: | ------: |
| ogro | fireball | 5 | 100 | 14 | 3 | 0 | 10 | 1 |
| rhino | chaingun | 7 | 70 | 18 | 2 | 70 | 1 | 2 |
| ratamahatta | shotgun | 50 | 120 | 12 | 1 | 100 | 30 | 4 |
| slith | rifle | 25 | 200 | 12 | 1 | 80 | 40 | 4 |
| bauul | rocket launcher | 30 | 500 | 10 | 1 | 0 | 20 | 6 |
| hellpig | bite | 12 | 50 | 22 | 3 | 0 | 10 | 1 |
| knight | iceball | 10 | 250 | 8 | 1 | 0 | 1 | 6 |
| goblin | slimeball | 7 | 100 | 12 | 1 | 0 | 20 | 2 |

The models are just temp until I find some real monster models.

* `damage` given is for a single shot, where damage is simply a quarter of the equivalent player gun. For the shotgun this is if he hits you at point blank range, but is a lot less at distance. Rocket launcher guy of course deals out splash damage too.
* `stamina` is the amount of damage taken before dying
* `speed` is their movement speed in units/sec (player = 24)
* `freq` is their relative frequency of spawning in DMSP mode.
* `lag` is how long it takes them to press fire after they have decided they will shoot in a particular direction, which means you can out-strafe their bullets (but still get hit when standing still or moving towards them). The number is in msec on skill 0, and half as big on skill 10.
* `rate` indicates how often they will attempt to shoot you while they have you in their sights. It is not an absolute fire rate measure, as fire rate is lower with distance and has some absolute minimum. Again, it is also dependant on skill (skill 10 fires twice as often as skill 0).
* `loyalty` indicates how often they have to be hit by another monster in a row before they fight back.

Other important parameters: monsters will at most jump down 4 units in height, unless their health is 100 or less, in which case they allways jump down.

As you can summise from the above, the keys to surviving in SP are: keep moving, preferably perpendicular to the monster line of sight, frequently move behind walls and back (this makes monsters search for you again, which delays them), and use monster infighting to your advantage. 

## Respawning

Classic SP works differently from most FPS games that employ a savegame based system. The developers believe that the problem with savegames is that they take away any tension in gameplay; since you play without fear because you can make frequent saves, and when you do have to reload, its just an annoyance (or frustration, if you forgot to save for a while because it was going so well). Because of this, savegames will not be added to the game.

Instead of savegames, Cube 2: Sauerbraten employs a novel system based on respawnpoints (not to be confused with checkpoints, which are just an annoying version of savegames). The major annoyance in other games comes from having to repeat the same thing, here, you can die, and still never have to repeat the same gameplay again, yet you still have strong motivation not to die. This brings back the tension in gameplay, without the frustration.

The way it works that when you die, the world stays AS IS. Dead monsters stay dead, and alive ones just continue at their current location. You respawn, as if it were Deathmatch, at your last respawnpoint. Respawnpoints are entities placed by the level designers in various spots throughout the level, and the game remembers the last one you touched. You can touch these more than once.

When you respawn, the evil monsters will have stolen your armour, and most of your ammo (currently they take 2/3rds, unless you have 5 or less, in which case they don't take anything). On the plus side, you will have all your health back (and you have kept any healthboost powerups!), and your starting supply of pistol ammo (see, the monsters are evil, but fair). Even though you are punished for dying, you are never stuck, since even if a group of monsters is very hard to overcome, they will be easier every time you try, since the dead ones stay dead (and the hurt ones stay hurt!). You may have to work with your pistol more, but that is part of the tradeoff.

An additional motivation to not die, is that deaths are the most important component in reaching a good score on a level. Every time you finish a level, you will see a printout like this:

```
--- single player time score: ---
time taken: 155 seconds
time penalty for 1 deaths (1 minute each): 60 seconds
time penalty for 21 monsters remaining (10 seconds each): 210 seconds
time penalty for lower skill level (20 seconds each): 140 seconds
time penalty for missed shots (1 second each %): 25 seconds
TOTAL SCORE (time + time penalties): 590 seconds (best so far: 423 seconds)
```

This gives you the stats of your game. The overall score is something that will be displayed in the menus next to the map name, so you can try and improve your best score, and compare to others. These score values are saved across runs of the game (in `bestscore_<mapname>` aliases).

The final formula has been tweaked such that:

* The number of deaths has the biggest impact on your score. You want to have 0 deaths for a good score.
* The second most important thing is frags, you want to make sure you have killed all monsters.
* The third most important thing is probably time... finish the map as quickly as you can
* another component is accuracy, which reflects shots fired compared to damage dealt (which is not the same as damage taken, i.e. it does not take into account if a rocket is mostly wasted because the monster only had 10 health).
* the last component is skill. If more agressive monsters don't phase you, then you can score more by playing on skill 10.
