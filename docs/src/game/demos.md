# Demos

You may record server-side demos during multiplayer games. You must have gained "admin" privileges by using the "setmaster" command to enable demo recording for a match. Once enabled, the next map that is played will start recording a demo. Once this map finishes, the server will provide the demo, so that any interested clients can download it. Demo recording is disabled automatically again for the next map, unless it is explicitly enabled again. The server will only store a fixed number of demos, and the oldest demo will be removed to make room for new ones if it already has the maximum number of demos (currently 5).

Demos may be played back via the special local "demo" mode (mode -1), where the map name is the name of the demo to be played.

### `/recorddemo [0/1]`
Sets whether demo recording will be enabled for the next match (B=1 to enable, 0 to disable). Requires admin privileges.

### `/stopdemo`
If in multiplayer, this command will finish recording a demo prematurely instead of waiting till the end of a match. Requires admin privileges.

If used during local demo playback, this will stop demo playback.

### `/cleardemos`
Clears all demos from the server. Requires admin privileges.

### `/listdemos`
This lists all demos available on the server. Any client may use this command.

### `/getdemo (N)`
This command retrieves a demo from the server, where N corresponds to the demo number provided by the "listdemos" command. If no number is specified, this command will get the most recent demo. The demo will be saved locally in the main directory as a file with the extension ".dmo". Any client may use this command.

### `/demo [F]`
This alias sets the mode to -1 (the special demo playback game mode) and map to F, where F is the name of a demo file, but without the ".dmo" file extension. The "stopdemo" command may be used to terminate playback early when in this game mode. 