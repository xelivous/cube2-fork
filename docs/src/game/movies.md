# Movies

Cube 2 can record movies as uncompressed video and sound to AVI files. These files will grow very large very rapidly and so it is strongly recommended that they are compressed before sharing.

### `/movie (filename)`

Providing a filename begins the movie recording, without a filename stops the recording. Small movies are stored as "<filename>.avi", but when a movie file exceeds 1Gb then the movies are stored as multiple files "<filename>XXX.avi", where XXX is numbered in sequence 000 to 999.

### `/moview [width]`
The width (default is 320) of the movie (0 will use the current window width). The movie will be downsized appropriately.

### `/movieh [height]`
The height (default is 240) of the movie (0 will use the current window height). The movie will be downsized appropriately.

### `/moviefps [framerate]`
The framerate (default is 24fps) at which to record the movie. Movie recording may prematurely stop if unable to record reliably at this framerate.

### `/moviesound [0/1]`
Enable recording of sound with 1, disable with 0.

### `/movieaccel [0/1]`
Enable GPU-accelerated video encoding with 1, disable with 0. This is enabled by default, but may cause problems on some video cards. If you notice movie recording displaying strange results, try disabling this and see if it helps. 