# Gameplay Commands

The following commands can be applied in the game by pressing T, and then typing /commandname. The backquote key (\`) is a shortcut for having the / typed for you. To have these commands applied automatically before the game starts open the autoexec.cfg file and add them there, without the /. Name and team in particular should be changed there.

### `/map [M]`
Loads a map. If connected to a multiplayer server, votes to load this map (others will have to type "map M" as well to agree with loading this map). To vote for a map with a specific mode, set the mode before you issue the map command (see multiplayer menu). See also map in the editref.
### `/name [yourname]`
Used for multiplayer, sets the name other players will know you as.

### `/team [teamname]`
Determines who are your teammates in any team game mode (truncated to 4 characters, case sensitive).

### `/say [text...]`
Outputs text to all players, as if you typed it.

### `/ignore [name]`
Ignores all chat messages from player `[name]`, where `[name]` is either the player's name or client number.

### `/unignore [name]`
Stops ignoring chat messages from player `[name]`, where `[name]` is either the player's name or client number.

### `/echo [text...]`
Outputs text to the console. Most useful for scripts.

### `/saycommand [P...]`
This puts a prompt on screen where you can type stuff into, which will capture all keystrokes until you press return (or esc to cancel). You may press tab to autocomplete commands/aliases/variables, and up/down keys to browse the command history. If what you typed started with a "/", the rest of it will be executed as a command, otherwise its something you "say" to all players. default key = T for talk, \` for commands. If P is prefix to insert to the buffer, (default key \` inserts a /).

### `/connect [serverip] (port (password))`
Connects to a server, e.g. "connect fov120.com". You can optionally specify a port for connecting to servers with custom ports. If port is 0 or not specified, then it connects to the default port. You can optionally specify a password for connecting to password-protected servers.

### `/lanconnect`
Connects to any server on the local area network by broadcasting the connection request.

### `/disconnect`
Leave the server.

### `/reconnect (password)`
Reconnects to the server which you were last connected to. You can optionally specify a password if the server was password-protected.

### `/rate [N]`
Sets your clients maximum downstream bandwidth to N kilobytes per second. Leaving it at 0 (the default) means the server will dynamically try to do the best thing, this is recommended for players who don't have a clue what their bandwidth is (setting your rate unoptimally is worse than not setting it all). Modem players (56k) could try rate 4 and tweak from there, dsl players can try rate 6-10. Note that the more players on a server, the more critical the effect of your rate.

### `/showscores`
+showscores turns display of scores (name/frags/network) on and -showscores turns it off. Default key = tab

### `/conskip [N]`
Allows you to browse through the console history, by offsetting the console output by N. Default key keypad - scrolls into the history (conskip 1) and keypad + resets the history (conskip -1000).

### `/toggleconsole`
Toggle between a more permanent and bigger console display and default, scroll off the screen variety. Unlike other games, you can play the game normally with the larger console on.

### `/weapon (a (b (c)))`
Tries to select weapons a, b & c, in that order, if they have ammo (0 = fist, 1 = sg, 2 = cg, 3 = rl, 4 = rifle, 5 = gl, 6 = pistol). If none of the 3 indicated have any ammo, it will try the remaining weapons for one with ammo (in order 3/2/1/4/0) (the fist needs no ammo). You can use the weapon command with 0 to 3 arguments. examples:

```
weapon 2 1 0 // close up selection
weapon 4 3 2 // far away selection
weapon 3 2 1 // medium distance selection
weapon 0 // humiliate selection :)
weapon // just switch to anything with ammo
```

In any selection, it will skip whichever weapon you have currently selected, so executing a weapon command repeatedly toggles between the two best weapons with ammo within a given selection. default keys 0 to 4 select the corresponding weapons, with sensible default alternatives, middle mouse button is a simple weapon switch.

### `/setweapon [N] (F)`
Sets with weapon to gun N, only if the gun has ammo. If F=1, then the gun is set to N, regardless of whether the gun has any ammo.

### `/cycleweapon (A (B (C (D (E)))))`
Cycles through the guns specified (may specify 1-5 guns). The next gun with ammo after the currently used gun in the sequence is selected.

### `/nextweapon (N) (F)`
Cycles through all available guns with ammo (or even empty guns if F=1 is specified). If N is -1, then it cycles through the available guns in reverse.

### `/gamespeed [P]`
Sets the gamespeed in percent, i.e. 50 is playing in slowmotion at half the speed. does not work in multiplayer. for entertainment purposes only :)

### `/mode [N]`
Set gameplay mode to N for the next game played (next map load). N can be:

|value|name|description|
|--:|---|---|
|0  | "ffa" / "default" mode | This is the default normal ffa game, and can also be used as "prewar" while setting up teams / voting for the next game. |
|1  | coop edit mode | This simply enables map editing in multiplayer, otherwise identical to mode 0.|
|2  | a standard teamplay game | will work with any number of teams with any number of players: you are allied with all players whose "team" setting is the same as yours. |
|3  | instagib mode | No items will spawn, but everyone will have 100 rifle rounds and 1 health.|
|4  | instagib team mode | Teams. No items will spawn, but everyone will have 100 rifle rounds and 1 health.|
|5  | efficiency mode | No items will spawn, but everyone will get all weapons with full ammo, and green armour.|
|6  | efficiency team mode | Teams. No items will spawn, but everyone will get all weapons with full ammo, and green armour.|
|7  | tactics mode | No items will spawn, but everyone will spawn with only base ammo for 2 random weapons and green armour.|
|8  | tactics team mode | Teams. No items will spawn, but everyone will spawn with only base ammo for 2 random weapons and green armour.|
|9  | capture mode | see capture mode section below.|
|10 | regen capture mode | like capture mode but with no respawn timer, and you regenerate health, armour, and ammo by standing on bases you own.|
|11 | ctf mode | Capture the flag where you must retrieve the enemy flag and return it to your own flag for points. |
|12 | insta ctf mode | Capture the flag as above, but with weapons, health, and items as in instagib mode. |
|13 | protect mode | Touch the enemy flag for points. Protect your own flag by picking it up. |
|14 | insta protect mode | Like protect mode above, but with weapons, health, and items as in instagib mode. |
|15 | hold mode | Hold the flag for a time to score points. |
|16 | insta hold mode | Like hold mode above, but with weapons, health, and items as in instagib mode. |
|17 | efficiency ctf mode | Capture the flag as above, but with weapons, health, and items as in efficiency mode. |
|18 | efficiency protect mode | Like protect mode above, but with weapons, health, and items as in efficiency mode. |
|19 | efficiency hold mode | Like hold mode above, but with weapons, health, and items as in efficiency mode. |
|20 | collect mode | Frag the enemy team to drop skulls. Collect them and bring them to the enemy base for points. |
|21 | insta collect mode | Like collect mode above, but with weapons, health, and items as in instagib mode. |
|22 | efficiency collect mode | Like collect mode above, but with weapons, health, and items as in efficiency mode. |
|-1 | demo playback mode | see demo recording section below. |
|-2, -3 | single player | see single player mode section below. |

Frag counting is the same for all modes: 1 for a frag, -1 for a suicide or a teamkill. Timelimit for all game modes is 10 minutes, except for coop-edit mode where there is no limit.

### `/skill N`
Sets the skill level (1-10) for single player modes. Default = 3. Affects number of monsters in DMSP mode, and general monster AI in all SP modes.

### `/showgui servers`
Displays the server menu. The server menu contains the last N servers you connected to, sorted by ping (servers are pinged automatically when bringing up this menu). Just select one to connect again. If you connect to a server by typing `/connect` manually, the server gets added here automatically. You can also add servers to `servers.cfg` manually.

### `/updatefrommaster`
Contacts the masterserver and adds any new servers to the server list (written to `servers.cfg` on exit). (see also multiplayer menu).

### `/clearservers (N)`
Clears all servers from the server browser's list. If N is 0 or not specified, only servers added by the masterserver are cleared. If N is 1, all servers are cleared, including kept servers.

### `/keepserver [NAME] [PORT] (PASSWORD)`
Adds a server to the server browser's list with ip address NAME and port PORT. If PASSWORD is specified, this password is used when clicking on the server in the server browser. This command causes the server to remain in the server browser's list even if the server list is updated from the master server or servers are cleared.

### `/addserver [NAME] [PORT] (PASSWORD)`
Adds a server to the server browser's list with ip address NAME and port PORT. If PASSWORD is specified, this password is used when clicking on the server in the server browser. Note that servers added to the list with this command will be removed when the list is updated from the masterserver!

### `/paused [B]`
Wether the game is paused or not (default 0, default key F1 toggles).

### `/blood [B]`
Toggles whether blood is enabled (default 1).

### `/damageblendfactor [F]`
The higher F, the longer the screen will stay red when you are damaged.

### `/damageblend [B]`
Toggles whether the screen is blended red when damaged (default 1).

### `/damagecompass [B]`
Toggles whether compass arrows are shown, indicating the amount and direction of damage taken (default 1).

### `/sendmap` `/getmap`
These two commands allow you to send other players maps they may not have while in multiplayer mode, and easily keep maps in sync while doing coop edit. "sendmap" reloads the current map you are on, then uploads it to the server and sends every other player a message about it. Other players can then simply type "getmap" to receive the current map, which is written to their local disk then reloaded. A second variant "sendmap name" is available which is particularly useful for coop editing, which first does a "savemap name" before performing the actual "sendmap". Thus in both cases you must already be on the map you want to send before issuing the command! (in some multiplayer that requires voting). Also note that "getmap" operates on the last map send by some other player, whatever it is. 