# Multiplayer

Quick start to multiplayer gaming:

1. Open up autoexec.cfg with an editor and make sure you have your name set to something unique.
1. Start the game
1. go to the multiplayer menu and select "update from master server", this will get you the lastest server list (you only need to do this once every day of multiplayer gaming or so).
1. select the server browser in the multiplayer menu. see how all servers get pinged automatically and information on player/map/game mode are displayed.
1. select a server with a good ping (near the top of the list, below 200 is very playable, above may give noticable delays on gameplay events but is still playable).
1. run, shoot and enjoy!
1. you may want to get familiar with the various game modes the game has, and how to vote for other maps and modes (again, easily done using the multiplayer menu) Be cooperative with the other players on the server, and vote. Remember, press T to talk to others.

Setting up a LAN game:

1. One of the computers on the LAN has to run server.bat. It really doesn't matter which.
1. The simplest way for all players to connect is for them to type "lanconnect" (if there is only one server on the LAN) or "connect servername" (if there are multiple servers) in the console (press ` to get in the console), where servername is the network name or ip address of the machine running server.bat. if all went well you should now be in the game together, as above.
1. If your LAN is connected to the internet, by default your server will register with the masterserver, and players could thus follow the procedure above to join the game: your server will show up in the list, and may thus be joined by people over the internet! If you want to avoid your server to contact the master server, you can start it with -mblah or something.

## Network Code

You will notice that the engine plays and responds better on a high ping connection than most games out there; the reason for this is that just about everything is done clientside, not just movement (as is common in games today) but also aim (the fat client / thin server model).

The benefits of this are that the effects of lag are minimised, and someone on a 200 ping can compete on an almost even playing field with someone with a 20 ping, the disadvantages are mainly in that its harder to combat cheating. Further advantages for server admins are that servers use virtually no cpu, and very little bandwidth for a lot of clients (you can host a 4 player game on a modem!).

It is impossible to completely hide lag however, and these are some of the effects that can still show lag if you or someone you play with has a high ping / bad connection:

* if the connection has packetloss.
    * There is no direct indicator of packetloss, instead the "packet jump" figure is provided on the hud (use "showscores"). Packet jump says as much as the amount of milliseconds that pass between updates from the indicated player. If it is extremely variable, or is high(>80) then your gameplay may be hampered (players jumping from place to place). Ideally it is a steady 40 or lower. A consistent packet jump is more vital to gameplay than ping. There is also player prediction based on extrapolation using the physics model, in an attempt to be both as up to date as possible in relation to the game state, and as realistic with regards to physics. However, under packet loss this model breaks down and player movement becomes choppy.
* if ping is high (either yours or someone you play with), some actions may appear lagged. The effect of ping is generally that the players in a game are effectively playing more and more seperate games, as someone with a high ping is "playing in the past". The local effects of this are masked out by client side movement & aim, but may become noticable across players:
    * you may get hit by someone even though you are already out of his view. This is because he is aiming at your movements of a while ago, and his hits take a while to arrive back at you. You may therefore be hit by gunfire up to his lag + your lag milliseconds after you leave his FOV.
    * item pickup: you may be denied an item if you try to pick it up at virtually the same time as someone else (server decides who was "first"). Item pickup is server side, only when you hear the sound effect have you actually picked up the item.
    * players dying appears lagged. This will improve.
* if a player is severely lagged (or you are) he temporarily becomes a "ghost" that cannot be hit (if >1sec packet jump).

## Online

Besides the abovementioned client-side gameplay, the code is also open source, which makes it too easy to cheat with. Anyone can modify the source to add cheats, recompile, and join multiplayer games.

There is no real trivial way to combat this:

* Moving the gameplay entirely serverside makes things a lot more complicated, hurts smoothness of gameplay, and still doesn't really solve the cheating problem
* Cube 2's predecessor, Cube, had "official binaries" which differed from the Open Source version in a different network protocol. This helped combat cheating somewhat, but still not entirely.
* A social solution, "Trusted Communities" was proposed, that would give players an identity, and only allow you to play on trusted servers if you were trusted by other players. This system could work well but requires a lot of infrastructure and organization

Cube and Cube 2 have rather small communities, and the main issue is not necessarily to combat cheating (a lost battle to start with), but more to allow non-cheaters to have undisturbed games together. Hence Sauerbraten multiplayer has a very simple solution:

There is no cheat protection at all. Nada.

Servers operate in a so called "master mode" which allows people to easily and painlessly have games with people they know, without being disturbed by cheaters. The way it works is very simple: the first person to enter a server and use the "setmaster" command becomes the "master" of that server. The important thing to see about these commands is that they don't prevent cheating, nor are necessarily a fair way of adminning a server. They just make it real easy to have fun games with friends without being disturbed by cheaters or people you don't want to play with.

In the event that a cheater is the master, or someone is being abusive with his masters powers, you can simply leave the server and start a game somewhere else. There are usually more servers than players anyway, and more players than cheaters.

If the current master leaves the server, other players may attempt to claim master status. Mastermode will be reset to 0 when this happens (also when the server clears). At any time, all players will see who is the master in a game because his name is marked in a different colour on the scoreboard. 

These are the commands available to the master:

### `/setmaster [0/1/password]`
Attempts to set master status. 0 gives up master status, 1 claims master status, or the admin password may be specified to steal master status from the current master. If you specify a password, you are granted "admin" status, which allows you to enable server features that an ordinary master can't.

### `/mastermode [mode]`
Sets the server master mode. `[mode]` can be (ranging from very open to very private):

| value | name | description |
| ----: | ---- | ----------- |
| 0 | open | anyone can enter the server. This is the default mode. It is good for games with random people, if no cheaters appear to be around. |
| 1 | veto | like 0, but now the master can force map changes. This is good when playing with new players who may not understand how to vote for map changes. |
| 2 | locked | like 1, but anyone joining after this mode has been set will be forced to be spectator only. This mode is ideal for tournament play, or games with friends only |
| 3 | private | like 2, but now no one can join the server anymore. Good for games with friends when cheaters are around, cheaters wont even connect, so wont have the chance to try and screw things up. |

### `/kick [number/name]`
Kick's the player with client number N (displayed in parentheses next to name) off the server, and bans his ip until 4 hours or the server is empty. You can't kick yourself. N may also just be the player's name. This command is useful when you started playing an open game, and a cheater joins. Additionally you can move to a higher mastermode for additional protection. Don't use this command for anything other than cheaters or people otherwise obstructing games.

### `/clearbans`
Clears all previously set bans, as if the server were empty.

### `/spectator [0/1] (N)`
Sets whether a player is a spectator (B=1 to enable, 0 to disable). N is specified as for kick. A player can voluntarily make himself a spectator, but only the current master can make other players spectators.

### `/goto [player]`

If you are a spectator, then this takes you to the location of player N (where N is a client number or the player's name).

### `/pausegame [0/1]`
Pauses a multiplayer game if N=1, or unpauses it if N=0. Requires admin privileges.