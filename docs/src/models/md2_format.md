# MD2 Format

 MD2 files must be located in a directory in packages/models/, you must provide a skin (either skin.jpg or skin.png) and the md2 itself (tris.md2). Optionally you may provide a masks.jpg that holds a specmap in the R channel, a glowmap in the G channel, and a chrome map in the B channel. The engine will apply it automatically.

If either of these files is not found, the engine will search the parent directory for them. For example, if for the flags/red model, the tris.md2 is not found in packages/models/flags/red/, then it will look for tris.md2 in packages/models/flags/. This allows the sharing of skins and geometry.

It is expected that md2 format files use Quake-compatible scale and animations, unless you configure the model otherwise.

You may also supply a config file (md2.cfg) in your model directory, which allows you to customize the model's animations. The following commands may be used in an md2.cfg:

md2anim N F M [S [P]]

N is the name of the animation to define. Any of the following names may be used:

    dying
    dead
    pain
    idle
    forward
    backward
    left
    right
    hold 1 ... hold 7
    attack 1 ... attack 7
    jump
    sink
    swim
    edit
    lag
    taunt
    win
    lose
    gun shoot
    gun idle
    vwep shoot
    vwep idle
    mapmodel
    trigger

F is the frame number the animation starts at. M is the number of frames in the animation. S is the optional frames per second at which to run the anim. If none is specified, 10 FPS is the default. P specifies an optional priority for the animation (defaults to P=0).

For example, defining a "pain" animation starting at frame 50 with 6 frames and running at 30 frames per second would look like: md2anim "pain" 50 6 30 