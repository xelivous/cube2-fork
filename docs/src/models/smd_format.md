# SMD Format

The Half-Life SMD format is configured almost identically to how MD5s are used. The only differences are that SMD specific commands are prefixed with "smd" instead of "md5" (i.e. "smdskin" instead of "md5skin") which are specified in "smd.cfg" instead of "md5.cfg".
