# IQM Format

The Inter-Quake Model (IQM) format is configured almost identically to how MD5s are used. The only differences are that IQM specific commands are prefixed with "iqm" instead of "md5" (i.e. "iqmskin" instead of "md5skin") which are specified in "iqm.cfg" instead of "md5.cfg". 