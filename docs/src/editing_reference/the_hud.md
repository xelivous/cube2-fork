# The HUD

 On the bottom left of the screen are a bunch of stats. You'll find out what they mean below.

    cube: the number of cubes in the current selection. Only the visible (leaf) cubes are counted.
    fps: frames per second
    ond: number of cubes in the system. This includes parent cubes, copied cubes, and undos.
    va: number of vertex arrays used to store vertices in the map. You probably don't need to use this.
    vtr: number of triangles currently being displayed. Will be a useful stat when culling is done.
    vvt: number of vertices currently being displayed. Useful when culling is done.
    tri: number of triangles in the entire map
    wvt: number of vertices in the entire map
    evt: misc rendering effects. Stuff like particles displayed, or text written on screen.

hidestats 0/1

Turn on to hide the above stats

hidehud 0/1

Turn on to hide all HUD elements 