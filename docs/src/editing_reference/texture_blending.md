# Texture blending

 Texture blending in maps is accomplished by smoothly blending two textures together to create variations on architecture and terrain.
Defining Blended Textures

Texture blending can use any two textures that are already set in your map.cfg, but you need to define the textures normally first. Texture thumbnails that have an additional texture layer to blend will have the texture they are set to blend with in one of the corners in the texture browser (f2).

To set up textures to blend, in your map cfg, under the texture that you want to have a second texture layer, you would add:

texlayer N

N is the index of the texture slot you want to use as the bottom texture layer to blend with. Texture slots start at 0, which is the first slot, 1 is the second slot, 2 is the third slot, etc. If N is a negative number, it will reference N slots back from the current slot, i.e. -1 references the previous texture slot.

vlayer N

Sets the bottom texture layer for all textures in the current selection, as if by "texlayer" command. However, negative values of N are not supported.
Blend Brushes

There will be a number of brushes already included and set up in the default "data/brush.cfg". If you want to set up your own brush, create a grayscale PNG file to use as a pattern. Keep in mind that the size of the brushes are relatively large, and not meant for super detailed work. So a 16 x 16 size image will make a brush that covers a very large patch of architecture.
Adding a New Brush

addblendbrush N F

Where N is the name of your brush, that you will use later to load the brush, and F is the file name, relative to the data directory. So if the brush you want to add is called "mybrush.png", then you would add the following line to your map.cfg file: addblendbrush mybrush "mybrush.png"". Note that the filename of the brush image is relative to the root directory.
Loading a Brush

setblendbrush N

Where N is the name of your brush that you defined in the cfg, or the name of an existing brush. So to set the brush to the one that was defined above, you would open the console with the tilde key and type: /setblendbrush mybrush

nextblendbrush N

Selects the next brush after the current one if N is not specified, or is 1. Otherwise, it will advance the current brush selected by N over the list of brushes. N may be negative, in which case it will go backwards in the list of brushes. By default this is bound to the scroll wheel.

rotateblendbrush

Rotates the current blend brush. By default this is bound to MOUSE2 (right mouse button).
Painting

First, texture the surfaces that you will want to paint with the textures that have had an extra texture layer defined for them in your map.cfg. You can only paint on textures that have had a second layer defined in the cfg. Now, you need to turn on a painting mode with the following command:

blendpaintmode N

Where N is a number from 0 to 5, which defines how the painting will be done. Setting it to 0 turns paint mode off. Usually you should paint with mode 2. By default each of these modes are bound to their corresponding number on the numeric keypad.

    0 - off
    1 - set - use for replacing/clearing
    2 - min(destination, source) - use for digging where black is the dig pattern
    3 - max(destination, source) - use for filling where white is the fill pattern
    4 - min(destination, invert(source)) - use for digging where white is the dig pattern
    5 - max(destination, invert(source)) - use for filling where black is the fill pattern

Experiment with the numbers to understand what they do. Finally, in order to paint on to the surface, you would open the console and type

paintblendmap

If you've done everything right, you should see your first blended textures. By default this is bound to MOUSE1 (left mouse button).

showblendmap

If for some reason the blendmap gets messed up while editing, you can use this command cause the blendmap to reshow without doing a full calclight.
Reverting Paints

There is NO UNDO for texture blending. If you've screwed something up, you can do one of two things. Open the console with the tilde key and type

clearblendmap

This will delete ALL of the texture blending for the ENTIRE level.

clearblendmapsel

This will clear the texture blending on the selected geometry.
Blending Hints and Tips

The amount of rendering passes the engine is required to make doubles on areas where the textures have been blended. 