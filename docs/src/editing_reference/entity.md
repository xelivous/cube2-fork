# Entity

newent type value1 value2 value3 value4

Adds a new entity where (x,y) is determined by the current selection (the red dot corner) and z by the camera height, of said type. Type is a string giving the type of entity, such as "light", and may optionally take values (depending on the entity). The types are defines below in the Entity Types section.

delent

deletes the selected entities

entflip

flip the selected entities. cube selection serves as both reference point and orientation to flip around.

entpush D

push the selected entities. cube selection serves as orientation to push towards.

entrotate D

rotate the selected entities in relation to the cube selection.

entmoving 0/1/2

set to 1 to turn on. if an entity is under the cursor when turned on, the entity will get toggled selected/unselected (set to 2 to add to selection instead of toggle). if selected, one can move the entity around using the cursor. if multiple entities are selected, they will also move. the plane on which the entity will be moved is dependent on the orientation of the cube surrounding the entity. set to 0 to turn off moving. if no entity is under the cursor when turned on, it will automatically turn off.

entdrop N

variable controlling where entities created with "newent" will be placed. N=0 place entities at your current eye position. N=1 drop entities to the floor beneath you. Lights, however, will be placed at your current eye position as for N=0. N=2 place entities at the center of the currently selected cube face. If a corner is selected, the entity will be placed at the corner vertex. N=3 behaves as with N=2, except all entities, including lights, will then bedropped from that position to whatever floor lies beneath. This mode is useful for placing objects on selected floors. Lights are also dropped to the floor, unlike for N=1.

dropent

Positions the selected entity according to the entdrop variable.

trigger T N

Sets the state of all locked triggers with tag T to N.

platform T N

If N = 0, stops all platforms or elevators with tag T. If N = 1, causes all platforms or elevators with tag T to move forwards or up, respectively. If N = -1, causes all platforms or elevators with tag T to move backwards or down, respectively.

entselect X

Takes a boolean expression as argument. Selects all entities that evaluate to true for the given expression. examples:

    entselect insel // select all entities in blue selection box
    entselect [ strcmp (et) "shells" ] // select all shells in map

entloop X

Loops through and executes the given expression for all selected entities. Note that most of the entity commands are already vector based and will automatically do this. Therefore they don't need to be explicitly executed within an entloop. Entloop is to be used when more precise custom instructions need to be executed on a selection.

Another property of entloop is that entity commands within it, that are normally executed on the entire selection, will only be done on the current entity iterator. In other words, the two following examples are equivalent:

    entset light 120 0 0 0
    entloop [ entset light 120 0 0 0 ]

The entset in the second statement will NOT be applied n squared times. Entloops can be nested.

entcancel

Deselect all entities.

enthavesel

Returns the number of entities in the current selection.

entget

Returns a string in the form of "type value1 value2 value3 value4". This string is the definition of the current selected entity. For example, the following statement will display the values of all the entities within the current selection:

    entloop [ echo (entget) ]

Outside of an entloop, the last entity selected will be returned. Normally, if an entity is highlighted by the cursor, it will only be considered as in the selection if an explicit one does not exisit (like cubic selections). However, entget is special in that it considers the highlighted entity as being the last entity in the selection. Entget is used to generate the default entdisplay at the bottom of the screen.

insel

Returns true if the selected entity is inside the cube selection

et

Cuts out the 'type' field from entget.

ea N

Cuts out the given 'value' field from entget. Attributes are numbered 0 to 3.

entset type value1 value2 value3 value4

Change the type and attributes of the selected entity. To quickly bring up the entset command in the console press '.' (default: period). It will come pre-filled with the values of the current entity selection (from entget).

entproperty P A

Changes property P (0..3) of the selected entities by amount A. For example "entproperty 0 2" when executed near a lightsource would increase its radius by 2.

entfind type value1 value2 value3 value4

Select all entities matching given values. '*' and blanks are wildcard. All ents that match the pattern will be ADDED to the selection.

clearents type

Deletes all entities of said type.

replaceents type value1 value2 value3 value4

Replaces the given values for the selected entity and all entities that are equal to the selected entity. To quickly bring up the replaceents command in the console press ',' (default: comma). It will come pre-filled with the values of the current entity selection.

entautoview N

Centers view on selected entity. Increment through selection by N. ex: N = 1 => next, N = -1 => previous. entautoviewdist N, sets the distance from entity. 