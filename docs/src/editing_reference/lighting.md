# Lighting

ambient R [G B]

This sets the level of ambient light (default: 25), where R G B are color values 0..255. If only R is specified, it is interpreted as a grayscale light value. This is the minimum amount of light that a surface will get, even when no light entities reach the surface.

edgetolerance N

This controls how far an intersection with some geometry has to be from the surface of the actual triangle in question before it counts as a shadow (default: 4), where N is 1..8; the distance immediately in front of the triangle along the edge where it ignores shadows.

When lighting a map, each lightmap pixel is the result of many samples from a grid-pattern, as a result the sampling pattern may go over the edge of one triangle but behind another. Increasing this variable helps alleviate this, especially in terrain heavy maps.

sunlight R [G B]

This sets the color of a simple directional sunlight. R G B are values in the range 0..255 (default: 0). If only R, is specified it is interpreted as a grayscale light value. Use "sunlight 0" to disable.

sunlightyaw YAW

This sets the yaw of the directional sunlight to YAW.

sunlightpitch PITCH

This sets the pitch of the directional sunlight to PITCH.

sunlightscale F

This sets the color scale of the direction sunlight to the floating-point value F (default: 1.0).

skylight R [G B]

This enables the skybox to "emit" light, that can be occluded by geometry or models in the map. A surface will cast a number of rays (currently 17), and any of them that hit the skybox will contribute a portion of the R G B light value above the "ambient" level to the surface (1/17th of the light). Effectively, the light will vary between the "ambient" value and the "skylight" value depending on how much of the skybox is visible. R G B are values in the range 0..255 (default: 0). If only R, is specified it is interpreted as a grayscale light value. Use "skylight 0" to disable.

lmshadows N

This controls the level of shadowing used when "calclight" or "patchlight" are not given a quality setting, where N is:

    2 (default): world and mapmodel shadows
    1: world shadows only
    0: no shadows

lmaa N

This controls the level of anti-aliasing used when "calclight" or "patchlight" are not given a quality setting, where N is:

    3 (default): 8xAA
    2: 4xAA
    1: 2xAA
    0: no AA

calclight Q

This calculates all lightmaps. Usually takes only a few seconds, depending on map size and settings. If you "savemap", the lightmap will be stored along with it. Q is these predefined quality settings:

    1: 8x anti aliasing, world and mapmodel shadows (maximum quality, slow)
    0 or not given: controlled by "lmshadows" and "lmaa" values
    -1: no anti aliasing, world shadows only (low quality, good for lighting previews)

patchlight Q

This will calculate lightmaps for any newly created cubes. This will generally be much quicker than doing a "calclight", and so is very useful when editing. However, it will make very inefficient use of lightmap textures, and any new cubes will not properly cast shadows on surfaces that are already lit. It is recommended you do a "calclight" on your map before you publish it with "savemap". A quality setting Q may be supplied, which behaves the same as for "calclight".

lightthreads N

This controls the number of threads (N) used by the "calclight" and "patchlight" commands You should set this variable to the number of processor cores you have to get a speed-up. By default (N=1), no multi-threading is used.

fullbright B

This variable controls whether the map will be shown with lighting disabled. Fullbright 1 will disable lighting, whereas 0 will enable lighting. (Default = 0)

lerpangle A

Default = 44. This variable controls whether surface normals are interpolated for lighting. Normals are sampled at each vertex of the surface. If the angle between two surfaces' normals (which meet at a vertex) is less than A, then the resulting normal will be the average of the two. Normals are then later interpolated between the normals at the vertexes of a surface.

lerpsubdiv N

Default = 2. This allows more normals to be sampled at points along an edge between two vertexes of a surface. 2^N-1 extra normals will be sampled along the edge, i.e. the edge is split in half for every increment of N.

lerpsubdivsize N

Default = 4. This sets the minimum size to which an edge may be subdivided. Edges smaller than N or edge sections smaller than N will not be sampled.

lightprecision P

Default = 32. This is the most important variable for tweaking the lighting, it determines what the resolution of the lightmap is. As such has a BIG effect on calculation time, video memory usage, and map file size. The default is good for most maps, you may go as low as 16 if you are lighting a really small map and love hard shadows, and for bigger maps you may need to set it to 64 or so to get reasonable memory usage.

The number to watch out for is the number of lightmaps generated which are shown on the HUD (and also as output after a calclight). 1 or 2 lightmap textures is very good, over 10 lightmap textures is excessive.

The map file size is 90% determined by the lightmaps, so tweak this value to get an acceptable quality to size ratio. Look at the size of the map files, sometimes a slightly higher lightprecision can halve the size of your .ogz.

Every surface matters, even though the engine attempts to compress surfaces with a uniform lightvalue, it is always a good ideas to delete parts of the world that are not part of your map. Lightprecision, lighterror, and lightlod are stored as part of map files.

lighterror E

There should be little reason to tweak this. If in your map you can see visible polygon boundaries caused by lighting, you can try stepping this down to 6 or 4 to improve quality at the expense of lightmap space. If you have an insanely large map and looking for ways to reduce file size, increasing error up to 16 may help. (Default = 8)

lightlod D

Default = 0. This will double the resolution of lightmaps (cut the lightprecision in half) if size of the surface being lit is smaller than 2^D units. This allows large maps to have pockets of detailed lighting without using a high resolution over everything. NOTE: if you feel like using this, test it thoroughly. On medium or small sized detailed maps, this command wastes space, use lightlod 0. Lightlod > 0 is only useful for huge maps

blurlms N

This variable controls whether to apply a blur filter to the lightmap after they are generated, as a post-pass. For N=0, no blur is applied. For N=1, a 3x3 blur filter is used. For N=2, a 5x5 blur filter is used. NOTE: this can cause lightmaps to mismatch at surface boundaries in complex scenes.

blurskylight N

This variable controls whether to apply a blur filter to the ambient skylight (enabled via the "skylight" command) before it is combined with the other lighting to create the final lightmap. This variable is useful for softening the skylight and making it appear more scattered. For N=0, no blur is applied. For N=1, a 3x3 blur filter is used. For N=2, a 5x5 blur filter is used.

dumplms

Dumps all lightmaps to a set of .bmps. Mostly interesting for developers, but mappers may find it interesting too. 