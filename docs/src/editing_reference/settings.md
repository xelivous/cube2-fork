# Settings

undomegs N

Sets the number of megabytes used for the undo buffer (default 8, max 100). Undo's work for any size areas, so the amount of undo steps per megabyte is more for small areas than for big ones.

showsky B

This variable controls whether explicit sky polygons are outlined (in purple) in edit mode. Default = 1.

outline B

This variable controls whether geometry boundaries (outlines) are shown. Default = 0.

wireframe 0/1

Turns on wireframe drawing of the map.

allfaces 0/1

when on, causes the texturing commands to apply the new texture to all sides of the selected cubes rather than just the selected face.

showmat B

This variables whether volumes are shown for invisible material surfaces in edit mode. Material volumes may also be selected while this is enabled. Default = 1.

optmats B

This variables controls whether material rendering should be optimized by grouping materials into the largest possible surfaces. This will always make rendering faster, so the only reason to disable it is for testing. Default = 1.

entselradius N

Sets the 'handle' size of entities when trying to select them. Larger sizes means it should be easier to select entities.

entselsnap 0/1

Turns on snap-to-grid while draggin entities. (default: 6)

entitysurf 0/1

When on, you will move with the entity as you push it with the scroll wheel. Of course, in order to push an entity, you must be holding it.

selectionsurf 0/1

When on, you will move with the selection box as you push it with the scroll wheel. Of course, in order to push a selection box, you must be holding it. 