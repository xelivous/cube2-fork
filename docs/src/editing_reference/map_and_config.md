# Map and config

map name

Loads up map "name" in the gamemode set previously by "mode". A map given as "blah" refers to "packages/base/blah.ogz", "mypackage/blah" refers to "packages/mypackage/blah.ogz". The menu has a set of maps that can be loaded. See also map in the gameplay docs.

At every map load, "data/default_map_settings.cfg" is loaded which sets up all texture definitions etc. Everything defined in there can be overridden per package or per map by creating a "package.cfg" or "mapname.cfg" which contains whatever you want to do differently from the default. It can also set up triggers scripts per map etc.

When the map finishes it will load the next map when one is defined, otherwise reloads the current map. You can define what map follows a particular map by making an alias like (in the map script): alias nextmap_blah1 blah2 (loads "blah2" after "blah1").

sendmap

Saves the current map (without lightmaps) and sends it to the server so other clients may download it. Only works in coopedit game mode.

getmap

Gets a map from the server if one is available. Automatically loads the map when done. Only works in coopedit game mode.

savemap name

savecurrentmap

Saves the current map, using the same naming scheme as "map". Makes a versioned backup to "mapname_N.BAK" if a map by that name already exists, so you can never lose a map. With "savemap", if you leave out the "name" argument, it is saved under the current map name. With "savecurrentmap", the map is saved with the name determined by the current game. Where you store a map depends on the complexity of what you are creating: if its a single map (maybe with its own .cfg) then the "base" package is the best place. If its multiple maps or a map with new media (textures etc.) its better to store it in its own package (a directory under "packages"), which makes distributing it less messy.

newmap size

Creates a new map of size^2 cubes (on the smallest grid size). 10 is a small map, 15 is a large map but it goes up to 20.

mapenlarge

Doubles the dimensions of the current map.

maptitle "Title by Author"

sets the map title, which will be displayed when the map loads. Either use the above format, or simply "by Author" if the map has no particular title (always displayed after the map load msg).

loadsky NAME [SPIN]

loads the skybox described by NAME, where NAME is a file name relative to the "packages/" directory. The engine will look for 6 sky box images: NAME_up.jpg, NAME_dn.jpg, NAME_lf.jpg, NAME_rt.jpg, NAME_ft.jpg, NAME_bk.jpg. These represent the skybox in the up, down, left, right, front, and back directions, respectively. If a .jpg file is not found, it will attempt to search for the files with a .png extension. SPIN, if specified, is floating point value that specifies, in degrees per second, the rate at which to spin/yaw the skybox. NOTE: This is an alias for the "skybox" and "spinsky" commands.

texturereset

Sets the texture slot to 0 for the subsequent "texture" commands.

materialreset

Resets the material texture slots for subsequent "texture" commands.

texture TYPE FILENAME ROT X Y SCALE

FILENAME 	Binds the texture indicated to the current texture slot, then increments the slot number depending on TYPE.
TYPE 	Specifying the primary diffuse texture advances to the next texture slot, while secondary types fill additional texture units in the order specified in the .cfg file. Allows secondary textures to be specified for a single texture slot, for use in shaders and other features, the combinations of multiple textures into a single texture are performed automatically in the shader rendering path:

    "c" or 0 for primary diffuse texture (RGB)
    "u" or 1 for generic secondary texture
    "d" for decals (RGBA), blended into the diffuse texture if running in fixed-function mode. To disable this combining, specify secondary textures as generic with 1 or "u"
    "n" for normal maps (XYZ)
    "g" for glow maps (RGB), blended into the diffuse texture if running in fixed-function mode. To disable this combining, specify secondary textures as generic with 1 or "u"
    "s" for specularity maps (grey-scale), put in alpha channel of diffuse ("c")
    "z" for depth maps (Z), put in alpha channel of normal ("n") maps
    "e" for environment maps (skybox), uses the same syntax as "loadsky", and set a custom environment map (overriding the "envmap" entities) to use in environment-mapped shaders ("bumpenv*world")

This may also be a material name, in which case it behaves like 0, but instead associates the slot with a material.
ROT 	Specifies preprocessing on the image, currently only rotation and flipping:

    0 = none
    1 = 90 CW
    2 = 180
    3 = 270 CW
    4 = X flip
    5 = Y flip

X and Y 	These are the X and Y offset in texels.
SCALE 	This will multiply the size of the texture as it appears on world geometry.

autograss FILENAME

Automatically generates grass for the current texture slot on any upward facing surface. The grass texture FILENAME is used to to texture the grass blades.

grassscale N

Sets the scaling of all grass textures to N where N=1..64 (default: 2).

grasscolour R G B

The colour of the grass, specified as R G B values from 0..255 (default: 255 255 255).

grassalpha A

Sets the opacity of all grass textures to A where floating point value A=0..1 (default: 1).

texscroll X Y

Scrolls the current texture slot at X and Y Hz, along the X and Y axes of the texture respectively.

texrotate N

Rotates the current texture slot by N*90 degrees for N=0..3. N=4 flips along the X axis, and N=5 flips along the Y axis.

vrotate N

Rotates all textures in the current selection as if by the "texrotate" command.

texoffset X Y

Offsets the current texture slot by X and Y texels along the X and Y axes of the texture respectively.

voffset X Y

Offsets all textures in the current selection as if by the "texoffset" command.

texscale N

Scales the current texture slot such that it is N times its normal size.

vscale N

Scales all textures in the current selection as if by the "texscale" command.

texalpha F B

Sets the alpha transparency of the front faces to F and back faces to B, where F and B are floating point values in the range 0.0 to 1.0. F defaults to 0.5, and B defaults to 0 (invisible).

valpha F B

Sets the alpha transparency of all textures in the current selection as if by the "texalpha" command.

texcolor R G B

Sets the color multiplier of the current texture slot to the color R G B, where R, G, and B are floating point values in the range 0.0 to 1.0. The default is white, i.e. (1.0 1.0 1.0).

vcolor R G B

Sets the color multiplier of all textures in the current selection as if by the "texcolor" command.

vreset

Resets the texture configuration of all textures in the current selection to the defaults defined in the map cfg file.

vdelta BODY

Excutes all of the "v*" commands in BODY such that they now only add to the current values for the textures in the current selection, rather than simply setting them. For example, vdelta [vrotate 1] would add 1 to the current rotation value for the textures, rather than just setting their rotation value to 1. This affects the "vrotate" (adds), "voffset" (adds), "vscale" (multiplies), "vshaderparam" (overrides), and "vcolor" (multiplies) commands.

fog N

Sets fog distance to N (default: 4000). You can do this for tweaking the visual effect of the fog, or if you are on a slow machine, setting the fog to a low value can also be a very effective way to increase fps (if you are geometry limited). Try out different values on big maps or maps which give you low fps.

fogcolour R G B

The colour of the fog, specified as R G B values from 0..255 (default: 128 153 179).

waterspec N

This sets the percentage of light water shows as specularity (default: 150).

waterfog N

Sets the distance beneath the surface of water at which it fogs, from 1..10000 (default: 150).

watercolour R G B

Sets the the colour of fog inside the water to the specified R G B value from 0..255 (default: 20 70 80). Used to give water some colour. Setting to the values 0 0 0 will cause it to reset to the default watercolour.

waterfallcolour R G B

Sets the the colour used for waterfall turbulence to the specified R G B value from 0..255 (default: 0 0 0). If the default of 0 0 0 is specified, waterfalls will use the colour supplied via "watercolour" instead.

lavafog N

Sets the distance beneath the surface of lava at which it fogs, from 1..10000 (default: 50).

lavacolour R G B

Sets the the colour of fog inside the lava to the specified R G B value from 0..255 (default: 255 64 0). Used to give lava some colour. Setting to the values 0 0 0 will cause it to reset to the default watercolour.

shader TYPE NAME VS PS

defines a shader NAME with vertex shader VS and pixel shader PS (both in ARB OpenGL 1.5 assembly format). See data/stdshader.cfg for examples. These definitions can be put in map cfg files or anywhere else, and will only be compiled once. TYPE indicates what resources the shader provides, or what backup method should be used if the graphics card does not support shaders. TYPE is either 0 for default shader, or 1 for normal-mapped world shaders. Requires DX9 / shader 2 class hardware (radeon 9500 or better, geforce 5200 or better) to run (older hardware will default to basic rendering).

fastshader NICE FAST N

Associates shader FAST so that it will run in place of shader NICE if shaderdetail is less than or equal to N.

setshader NAME

Sets a previously defined shader as the current shader. Any following texture slots (see "texture" command) will have this shader attached to them. Any pixel or vertex parameters are reset to the shader's defaults when this command is used.

setshaderparam NAME X Y Z W

Overrides a uniform parameter for the current shader. Any following texture slots will use this pixel parameter until its value is set/reset by subsequent commands. NAME is the name of a defined parameter of the current shader. It's value is set to the vector (X, Y, Z, W). Coordinates that are not specified default to 0.

vshaderparam NAME X Y Z W

Overrides a uniform parameter for the shaders of all textures in the current selection, as if by the "setshaderparam" command.

setpixelparam INDEX X Y Z W

Overrides a pixel parameter for the current shader. Any following texture slots will use this pixel parameter until its value is set/reset by subsequent commands. INDEX is the index of a program environment parameter (program.env[10+INDEX]) to the pixel program of the current shader. It's value is set to the vector (X, Y, Z, W). Coordinates that are not specified default to 0.

setvertexparam INDEX X Y Z W

Overrides a vertex parameter for the current shader. Any following texture slots will use this vertex parameter until its value is set/reset by subsequent commands. INDEX is the index of a program environment parameter (program.env[10+INDEX]) to the vertex program of the current shader. It's value is set to the vector (X, Y, Z, W). Coordinates that are not specified default to 0.

setuniformparam NAME X Y Z W

Overrides a uniform parameter for the current shader. Any following texture slots will use this pixel parameter until its value is set/reset by subsequent commands. NAME is the name of a uniform variable in the current GLSL shader. It's value is set to the vector (X, Y, Z, W). Coordinates that are not specified default to 0.
Shader 	Shader params 	Texture slots 	
stdworld 		c 	The default lightmapped world shader.
decalworld 		c, d 	Like stdworld, except alpha blends decal texture on diffuse texture.
glowworld 	

    glowcolor: Rk, Gk, Bk - multiplies the glow map color by the factors Rk, Gk, Bk

	c, g 	Like stdworld, except adds light from glow map.
bumpworld 		c, n 	Normal-mapped shader without specularity (diffuse lighting only).
bumpglowworld 	

    glowcolor: Rk, Gk, Bk - multiplies the glow map color by the factors Rk, Gk, Bk

	c, n, g 	Normal-mapped shader with glow map and without specularity.
bumpspecworld 	

    specscale: Rk, Gk, Bk - multiplies the specular light color by the factors Rk, Gk, Bk

	c, n 	Normal-mapped shader with constant specularity factor.
bumpspecmapworld 	same as above 	c, n, s 	Normal-mapped shader with specularity map.
bumpspecglowworld 	

    glowcolor: Rk, Gk, Bk - multiplies the glow map color by the factors Rk, Gk, Bk
    specscale: Rk, Gk, Bk - multiplies the specular light color by the factors Rk, Gk, Bk

	c, n, g 	Normal-mapped shader with constant specularity factor and glow map.
bumpspecmapglowworld 	same as above 	c, n, s, g 	Normal-mapped shader with specularity map and glow map.
bumpparallaxworld 	

    parallaxscale: Scale, Bias - Scales the heightmap offset

	c, n, z 	Normal-mapped shader with height map and without specularity.
bumpspecparallaxworld 	

    specscale: Rk, Gk, Bk - multiplies the specular light color by the factors Rk, Gk, Bk
    parallaxscale: Scale, Bias - Scales the heightmap offset

	c, n, z 	Normal-mapped shader with constant specularity factor and height map.
bumpspecmapparallaxworld 	same as above 	c, n, s, z 	Normal-mapped shader with specularity map and height map.
bumpparallaxglowworld 	

    glowcolor: Rk, Gk, Bk - multiplies the glow map color by the factors Rk, Gk, Bk
    parallaxscale: Scale, Bias - Scales the heightmap offset

	c, n, z, g 	Normal-mapped shader with height and glow maps, and without specularity.
bumpspecparallaxglowworld 	

    glowcolor: Rk, Gk, Bk - multiplies the glow map color by the factors Rk, Gk, Bk
    specscale: Rk, Gk, Bk - multiplies the specular light color by the factors Rk, Gk, Bk
    parallaxscale: Scale, Bias - Scales the heightmap offset

	c, n, z, g 	Normal-mapped shader with constant specularity factor, and height and glow maps.
bumpspecmapparallaxglowworld 	same as above 	c, n, s, z, g 	Normal-mapped shader with specularity, height, and glow maps.
bumpenv* 	

    envscale: Rk, Gk, Bk - multiplies the environment map color by the factors Rk, Gk, Bk

		Any of the above bump* shader permutations may replace "bump" with "bumpenv" (i.e. bumpenvspecmapworld), and will then reflect the closest envmap entity (or the skybox if necessary). They support all their usual texture slots and pixel params, in addition to the envmap multiplier pixel param. If a specmap is present in the given shader, the raw specmap value will be scaled by the envmap multipliers (instead of the specmap ones), to determine how much of the envmap to reflect. A calclight (if it has not been done before) or recalc (thereafter) is also needed by this shader to properly setup its engine state.

music name [ondone]

Plays song "name" (with "packages" as base dir). This command is best used from map cfg files or triggers. Evaluates ondone when the song is finished, or just keeps looping the song if ondone is missing. Example: music "songs/music.ogg" [ echo "Song done playing!" ]

N = registersound name V

Registers sound "name" (see for example data/sounds.cfg). This command returns the sound number N, which is assigned from 0 onwards, and which can be used with "sound" command below. if the sound was already registered, its existing index is returned. registersound does not actually load the sound, this is done on first play. V is volume adjustment; if not specified (0), it is the default 100, valid range is 1-255.

sound N

Plays sound N, see data/sounds.cfg for default sounds, and use registersound to register your own. for example, sound 0 and sound (registersound "aard/jump") both play the standard jump sound.

mapsound name V N

Registers sound "name" as a map-specific sounds. These map-specific sounds may currently only be used with "sound" entities within a map. The first map sound registered in a map has index 0, and increases afterwards (the second sound in the config is 1 and the third is 2, and so on). V is volume adjustment; if not specified (0), it is the default 100, valid range is 1-255. N is the maximum number instances of this sound that are allowed to play simultaneously; the default is only 1 instance. If N is -1, then an unlimited number of instances may play simultaneously.

mapmodel R H T N SH

mmodel N

NOTE: the mapmodel form is deprecated.. set additional properties of a mapmodel in its .cfg Registers a mapmodel that can be placed in maps using newent mapmodel (see newent). N is the name, R is the square radius, H the height, T the texture slot to skin the model with (0 for default skin), and SH toggles whether the it will cast shadows (not given or 1 casts shadows, 0 has no shadows). The radius R and height H define the collision bounding box of the model (if either is 0, players won't collide with the mapmodel). Name N is the name of a folder inside packages/models folder, e.g. "hudguns/rifle". Loaded from that folder are: tris.md2 and skin.jpg (and if not available, skin.png, or the same from the parent folder to allow skin sharing).

Example: mapmodel 4 32 0 "tree1"

This map model is loaded from packages/models/tree1/. It has a collision box 8x8x32 in size (x=2*R, y=2*R, z=H). It uses the model's default skin (texture slot=0). It casts shadows (default).

shadowmapambient N

specifies a colour to use for the ambient light value of shadows created by shadowmapping, where N is a hexadecimal colour value of the form "0xRRGGBB". Note that any value of 255 or less are treated as gray-scale. If N is 0 or unset, this value is determined by the "ambient" variable and the "skylight" command. (Default: 0)

shadowmapangle N

specifies the angle in degrees at which shadows created by shadowmapping point. If N is 0 or unset, this value is guessed based on any radius 0 lights in the map.

causticscale N

specifies the scale, as a percent, to multiply the size of water caustics by.

causticmillis N

specifies the speed at which water caustics play, in milliseconds per frame.

skybox NAME

loads the skybox described by NAME, where NAME is a file name relative to the "packages/" directory. The engine will look for 6 sky box images: NAME_up.jpg, NAME_dn.jpg, NAME_lf.jpg, NAME_rt.jpg, NAME_ft.jpg, NAME_bk.jpg. These represent the skybox in the up, down, left, right, front, and back directions, respectively. If a .jpg file is not found, it will attempt to search for the files with a .png extension.

spinsky SPIN

a floating point value that specifies, in degrees per second, the rate at which to spin/yaw the skybox.

yawsky YAW

specifies in degrees a constant yaw rotation to apply to the skybox.

cloudbox NAME

loads the cloudbox described by NAME, similar to the "skybox" command. The cloudbox should have an alpha channel which is used to blend it over the normal skybox.

spinclouds SPIN

a floating point value that specifies, in degrees per second, the rate at which to spin/yaw the cloudbox.

yawclouds YAW

specifies in degrees a constant yaw rotation to apply to the cloudbox.

cloudclip CLIP

specifies a vertical offset at which to clip the cloudbox, a floating point value between 0 and 1. This defaults to 0.5, meaning the bottom half of the cloudbox is clipped away.

cloudlayer NAME

loads the cloud layer described by NAME, where NAME is a file name relative to the "packages/" directory. The engine will look for either "packages/NAME.png" or "packages/NAME.jpg". The cloud layer should have an alpha channel which is used to blend it onto the skybox. The cloud layer is mapped onto a horizontal circle that fades into the edges of the skybox.

cloudscrollx N

specifies the rate, a floating-point value in Hz, at which the cloud layer scrolls in the X direction.

cloudscrolly N

specifies the rate, a floating-point value in Hz, at which the cloud layer scrolls in the Y direction.

cloudscale N

specifies the scale as a floating-point value telling how much to multiply the size of the cloud layer. (Default: 1)

cloudheight N

specifies the height of the cloud layer as a floating-point value, where -1 corresponds to the bottom of the skybox, 0 corresponds to the middle of the skybox, and 1 corresponds to the top of the skybox. Intermediate values place the cloud layer at intermediate heights of those. (Default: 0.2)

cloudfade N

specifies the offset towards the center of the cloud layer at which the cloud layer will start fading into the skybox. This is a floating-point value between 0 and 1, where 0 corresponds to the edge of the cloud layer, and 1 corresponds to the center. (Default: 0.2)

cloudcolour N

specifies a colour multiplier for the cloud layer, where N is a hexadecimal colour value in the form of "0xRRGGBB". (Default: 0xFFFFFF, white)

cloudalpha A

specifies an opacity for the cloud layer, where A is a floating-point value between 0 and 1. (Default: 1, solid)

fogdomeheight N

specifies the height of the fog dome as a floating-point value, where -1 corresponds to the bottom of the skybox, 0 corresponds to the middle of the skybox, and 1 corresponds to the top of the skybox. Intermediate values place the fog dome at intermediate heights of those. (Default: -0.5)

fogdomemin A

specifies a minimum opacity for the fog dome, where A is a floating-point value between 0 and 1. (Default: 0, invisible)

fogdomemax A

specifies a maximum opacity for the fog dome, where A is a floating-point value between 0 and 1. (Default: 0, invisible)

fogdomecap B

specifies whether the bottom of the fog dome should be capped, where B is 0 or 1 (Default: 1, on).

fogdomeclip Z

specifies whether the top of the fog dome should be clipped off at a relative size Z, where Z is a floating-point value between 0 and 1 (Default: 1, not clipped).

fogdomecolour R G B

The colour of the fog dome, specified as R G B values from 0..255 (default: 0 0 0). If the colour is 0, then the value of fogcolour is used instead of fogdomecolour.

skytexture B

specifies whether or not to enable rendering of sky-textured surfaces. If set to 0, sky-textured surfaces are not rendered, allowing sky texture to be used as a "don't render this" surface. Disabling this also allows the skybox to be rendered last after the scene, which yields speedups on some video cards, so disable this if possible in your map, even though it defaults to on. (Default: 1)

importcube N

Imports a cube map (.cgz) and converts it to the new OCTA (.ogz) map format. N is the name of the map, without the .cgz. The map file must reside in packages/cube, which is because that folder has a package.cfg that sets the default cube textures. If the cube map in question has a custom texture list, it will have to be adapted manually. Currently converts everything relatively faithfully, except heighfields which are converted as best as possible but not always accurately. Slopes tend to work faultlessly, landscape style stuff is usuable, but curves/arches are problematic, and may have to be redone. All entities are converted though mapmodels may not be present, and light entities are useless because of their 2d nature, so probably the first thing to do after converting a map is /clearents light, and place some new lights. Pickups and other items may spawn inside the walls because they have no proper Z value, you may have to correct these manually. The importcube command does not automatically save the map, you still have to do a /savecurrentmap which will create packages/cube/N.ogz. Reload the map to be able to pick up stuff. Waterlevel is also not supported, you will have to add water using the new material system.

writeobj N

Writes out the current map as N.obj, so you could use the engine as a generic modeller with any program/engine that uses meshes. The meshes aren't very optimal and don't have texture/lighting information.

flipnormalmapy D N

Normalmaps generally come in two kinds, left-handed or righ-handed coordinate systems. If you are trying to use normalmaps authored for other engines, you may find that the lighting goes the wrong way along one axis, this can be fixed by flipping the Y coordinate of the normal. This command loads normalmap N (MUST be 24bit .tga), and writes out a flipped normalmap as D (also tga).

mergenormalmaps H N

Normalmaps authored for Quake 4 often come as a base normal map, with seperate height offset file *_h.tga. This is NOT a height file as used for parallax, instead its detail to be blended onto the normals. This command takes normalmap N and a _h file H (both MUST be 24bit .tga), and outputs a combined normalmap N (it *overwrites* N). 