# Editing

edittoggle

Switches between map edit mode and normal (default key: e). In map edit mode you can select bits of the map by clicking or dragging your crosshair on the cubes (using the "attack" command, normally MOUSE1), then use the commands below to modify the selection. While in edit mode, physics & collision don't apply (noclip), and key repeat is ON.

dragging 0/1

Select cubes when set to 1. stop selection when set to 0

editdrag

Select cubes and entities. (default: left mouse button)

selcorners

Select the corners of cubes. (default: middle mouse button)

moving 0/1

set to 1 to turn on. when on, it will move the selection (cubes not included) to another position. the plane on which it will move on is dependent on which side of the selection your cursor was on when turned on. set to 0 to turn off moving. if cursor is not on selection when turned on, moving will automatically be turned off.

editmovedrag

if cursor is in current cube selection, holding will move selection. otherwise will create new selection.

cancelsel

Cancels out any explicit selection you currently have (default: space).

editface D N

This is the main editing command. D is the direction of the action, -1 for towards you, 1 for away from you (default: scroll wheel). N=0 to push all corners in the white box (hold F). N=1 create or destroy cubes (default). N=2 push or pull a corner you are pointing at (hold Q).

gridpower N

Changes the size of the grid. (default: g + scrollwheel)

edittex D

Changes the texture on current selection by browsing through a list of textures directly shown on the cubes. D is the direction you want to cycle the textures in (1 = forwards, -1 = backwards) (default: y + scrollwheel). The way this works is slightly strange at first, but allows for very fast texture assignment. All textures are in a list. and each time a texture is used, it is moved to the top of the list. So after a bit of editing, all your most frequently used textures will come first, and the most recently used texture is set immediately when you press the forward key for the type. These lists are saved with the map.

gettex

moves the texture on the current selection to the top of the texture list. Useful for quickly texturing things using already textured geometry.

selextend

Extend current selection to include the cursor.

passthrough

normally cubes of equal size to the grid are given priority when selecting. passthrough removes this priority while held down so that the cube the cursor is directly on is selected. Holding down passthrough will also give priority to cube over entities. (default: alt)

reorient

Change the side the white box is on to be the same as where you are currently pointing. (default: shift)

flip

Flip (mirror) the selected cubes front to back relative to the side of the white box. (default: x)

rotate D

Rotates the selection 90 degrees around the side of the white box. Automatically squares the selection if it isn't already. (default: r + scroll wheel)

undo

Multi-level undo of any of the changes caused by the above operations (default: z [or u]).

redo

Multi-level redo of any of the changes caused by the above undo (default: i).

copy

paste

Copy copies the current selection into a buffer. Upon pressing 'paste', a selection box will be created to identify the location of the pasted cubes. Releasing the 'paste' button will actually paste the cubes. So combined with the 'moving' command you can easily place and clone sets of cubes. If the current gridsize is changed from the copy, the pasted copy will be stretched by the same factor.

editcopy

editpaste

Will copy cubes as normal copy, but also features entity copies. There are three different methods of use:

    If no entity is explicitly selected, editcopy will copy the selected cube, just like normal the normal 'copy' command.
    If one or more entities are selected, editcopy will copy the last entity selected. Editpaste will create a new entity using copy as the template if no entities are selected, otherwise it will overwrite all selected entities with the copied ent.
    If there are both entity and cube selections, editcopy will copy the entire selection. In other words, when editpaste is used it will paste the cube selection along with all of the entities that were selected.

replace

Repeats the last texture edit across the whole map. Only those faces with textures matching the one that was last edited will be replaced.

replacesel

Repeats the last texture edit only within the currently selected region. Only those faces with textures matching the one that was last edited will be replaced.

editmat MAT [FILTER]

Changes the type of material left behind when a cube is deleted to MAT. If FILTER is specified, then only cubes with that material named by FILTER are changed to MAT. MAT may also be "", indicating that only those parts of the material mask matching FILTER will be cleared, as opposed to setting MAT to "air", which would clear the entire material mask. FILTER may alternatively be one of "empty", "notempty", "solid", and "notsolid" which will then only affect cubes containing such geometry.

Currently the following types of materials are supported:

    air: the default material, has no effect. Overwrites other volume materials.
    water: acts as you would expect. Renders the top as a reflection/refraction and the sides as a waterfall if it isn't contained. Should be placed with a floor at the bottom to contain it. Shows blue volume in edit mode. Overwrites other volume materials.
    glass: a clip-like material with a blended/reflective surface. Glass also stops bullets. Will reflect the closest envmap entity, or if none is in range, the skybox. Shows cyan volume in edit mode. Overwrites other volume materials.
    lava: renders the top as a glowing lava flow and the sides as lavafalls if it isn't contained. It kills any players who enter it. Shows orange volume in edit mode. Overwrite other volume materials.
    clip: an invisible wall that blocks players movement but not bullets. Is ideally used to keep players "in bounds" in a map. Can be used sparingly to smooth out the flow around decoration. Shows red volume in edit mode. Overwrites other clip materials.
    noclip: cubes are always treated as empty in physics. Shows green volume in edit mode. Overwrites other clip materials.
    gameclip: a game mode specific clip material. Currently it can block monsters in SP modes, it can stop flags from being picked up in areas in CTF/protect modes, and it can stop capturing of bases in areas in capture modes. Overwrites other clip materials.
    death: causes the player to suicide if he is inside the material. Shows black volume in edit mode.
    alpha: all faces of cubes with this material are rendered transparently. Use the "valpha" and "texalpha" commands to control the transparency of front and back faces. Shows pink volume in edit mode.

recalc

Recalculates scene geometry. This also will regenerate any envmaps to reflect the changed geometry, and fix any geometry with "bumpenv*" shaders to use the closest available envmaps. This command is also implicitly used by calclight.

havesel

Returns the number of explicitly selected cubes for scripting purposes. Returns 0 if the cubes are only implicitly selected.

gotosel

Goes to the position of the currently selected cube or entity.

savebrush B

Saves the current selection as an octa-brush named B.

pastebrush B

Pastes the octa-brush named B. 