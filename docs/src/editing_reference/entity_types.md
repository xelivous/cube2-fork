# Entity types

 Entities are shown in editmode by blue sparklies, and the closest one is indicated on the HUD.

"light" radius r g b

If G and B are 0 the R value will be taken as brightness for a white light. A good radius for a small wall light is 64, for a middle sized room 128... for a sun probably more like 1000. Lights with a radius of 0 do not attenuate and may be more appropriate for simulating sunlight or ambient light; however, this comes at the cost of slightly greater map file sizes. See the lighting commands for an indepth list of all lighting related commands.

"spotlight" radius

Creates a spotlight with the given "radius" (in degrees, 0 to 90). A 90 degree spotlight will be a full hemisphere, whereas 0 degrees is simply a line. These will attach to the nearest "light" entity within 100 units of the spotlight. The spotlight will shine in the direction of the spotlight, relative to the "light" entity it is attached to. It inherits the sphere of influence (length of the spotlight) and color values from the attached light as well. Do not move these very far from the light they're or attached to or you risk them detaching or attaching to the wrong lights on a map load!

"envmap" [radius]

Creates an environment map reflecting the geometry around the entity. The optional radius overrides the maximum distance within which glass or geometry using the "bumpenv*" shaders will reflect from this environment map. If none is specified, the default is taken from the variable "envmapradius" (which defaults to 128 units), which may also be set in map cfgs. Environment maps are generated on a map load, or can be regenerated while editing using the "recalc" command. Please use the absolute minimum number of these possible. Each one uses up a decent amount of texture memory. For instance, rather than using two environment maps on each side of a window, use only one in the middle of the pane of glass. If you have a wall with many windows, place only one environment map in the middle of the wall geometry, and it should work just fine for all the windows.

"sound" N radius [size]

Will play map-specific sound N so long as the player is within the radius. However, only up to the max uses allowed for N (specified in the mapsound command) will play, even if the player is within the radius of more N sounds than the max. By default (size 0), the sound is a point source. Its volume is maximal at the entity's location, and tapers off to 0 at the radius. If size is specified, the volume is maximal within the specified size, and only starts tapering once outside this distance. Radius is always defined as distance from the entity's location, so a size greater than or equal to the radius will just make a sound that is always max volume within the radius, and off outside.

"playerstart" [Y] [T]

Spawn spot, yaw Y is taken from the current camera yaw (should not be explicitly specified to "newent"). If T is specified, then the playerstart is used as a team-spawn for CTF modes ONLY, where team T may be either 1 or 2, matching the parameter supplied to "flag" entities. For all other modes, team T should either be 0 or simply not specified at all! Note that normal playerstarts are never used for CTF, and CTF playerstarts are never used for spawns in other modes.

"flag" [Y] T

A team flag for CTF maps ONLY. Yaw Y is taken from the current camera yaw (should not be explicitly specified to "newent"). Team T may be either 1 or 2. Playerstarts with a matching team will be chosen in CTF mode.

"base" [ammo [N]]

A base for capture mode. If N is specified, the alias "base_N" will be looked up, and its value used for the name of the base, or otherwise a default name will be assigned. If ammo is specified, the base will always produce that type of ammo. If ammo is unspecified or 0, the server will randomly choose a type of ammo to produce at the start of the match. If ammo is negative, then it will pick a random type, but will match all other bases with the same negative ammo value. Ammo types are:

    1: shells (shotgun)
    2: bullets (chaingun)
    3: rockets (rocket launcher)
    4: rifle rounds (rifle)
    5: grenades (grenade launcher
    6: cartridges (pistol)

"shells"

"bullets"

"rockets"

"riflerounds"

"grenades"

"cartridges"

"health"

"healthboost"

"greenarmour"

"yellowarmour"

"quaddamage"

A variety of pickup-able items, see here.

"teleport" N [M]

"teledest" [Y] N [P]

Creates a teleport connection, teleports are linked to a teledest with the same N (of which there should be exactly one). N can be 0..255. Y is the yaw of the destination and is initially taken from the current camera yaw, it cannot be specified when creating the entity. If M is 0 or not specified, the default teleporter model is used. If M is -1, no model is rendered for the teleporter. If M is a value 1 or greater, the corresponding mapmodel slot is used as the teleporter's model. If P is 1, the teledest preserves the velocity of the player, otherwise the player's velocity is reset upon teleporting.

"jumppad" Z [Y] [X]

A jumppad entity which gives you a push in the direction specified. For example, "jumppad 30 5" makes you bounce up quite a bit and also pushes you forward a bit (so it is easier to land on a higher platform). This entity does not render anything, so you are responsible for creating something that looks logical below this entity.

"mapmodel" [Y] N T R

A map model, i.e. an object rendered as md2/md3 which you collide against, cast shadows etc. Y is the yaw of the model and is initially taken from the current camera yaw, it cannot be specified when creating the entity. N determines which mapmodel you want, this depends on "mapmodel" declarations in the maps cfg file. T specifies mapmodel behaviour such as triggers, see table below. R is the trigger number, 0 means no trigger. This number specifies what trigger to activate, and in addition, the alias "level_trigger_Trigger" will be executed, where Trigger is substituted accordingly (this allows you to script additional actions upon a trigger, i.e. put this into your map cfg file to print a message: alias level_trigger_1 "echo A door opened nearby"). The alias "triggerstate" will hold a value of -1, 0, or 1 indicating how the trigger was activated.
Type 	Trigger states 	Trigger how often 	Sound 	
0 		0 		loops mapmodel animation
1 		1 		do trigger animation when touched for the first time only and return to starting position (best for switches, use switch/lever models)
2 		1 	rumble 	same as above but with sound
3 	toggle (0/1) 	1 		do trigger animation when touched for the first time only and stay in toggled position (best for switches, use switch/lever models)
4 	toggle (0/1) 	1 	rumble 	same as above but with sound
5 		N 		do trigger animation when touched every time and return to starting position (best for switches, use switch/lever models)
6 		N 	rumble 	same as above but with sound
7 	toggle (0/1) 	N 		do trigger animation when touched every time and toggle positions (best for reversible switches, use switch/lever models)
8 	toggle (0/1) 	N 	rumble 	same as above but with sound
9 	closed/open (0/1) 	1 	door? 	opened by approach first time only, stays open afterwards. Collides while closed. (use door specific models)
10 	closed/open (0/1) 	N 	door? 	opened by approach every time, closes after 5 seconds. Collides while closed. (use door specific models)
11 	locked (-1) 	0/N 	door? 	opened/closed only by associated trigger. When approached while closed, collides and invokes level trigger with triggerstate -1. (use door specific models)
12 	disappear (0) 	1 		do trigger animation once when touched, disappear after (good for triggers that look more like pickups, such as the carrot)
13 	disappear (0) 	1 	rumble 	same as above but with sound
14 	disappear (-1) 	0 		like 11, but disappears after opening.
29 	disappear (0) 	1 	end? 	FPS specific. END LEVEL

Be careful when using "switch many" for thing that affect gameplay, such as opening doors, as it can be confusing. Best is to reserve a particular model to mean "many" and others "once". All types >0 are snapped to 15 degree angles for orientation.

"box" [Y] N W

"barrel" [Y] N W H

Like a mapmodel, except that damage done to it will push the entity around. Y is the yaw of the model and is initially taken from the current camera yaw, it cannot be specified when creating the entity. N determines which mapmodel you want, this depends on "mapmodel" declarations in the maps cfg file. W is the weight of the box or barrel where the heavier it is the less it moves; if W is not specified or 0, it defaults to 25. Barrels, unlike boxes, will explode if more than H damage is done to them; if H is not specified or 0, it defaults to 50.

"platform" [Y] N T S

"elevator" [Y] N T S

Like a mapmodel, except it moves around and carries players, monsters, or other dynamic entities. Y is the yaw of the model and is initially taken from the current camera yaw, it cannot be specified when creating the entity. N determines which mapmodel you want, this depends on "mapmodel" declarations in the maps cfg file. A platform will travel horizontally back and forth along the direction of its yaw, while an elevator will travel only up and down. When they hit an obstacle such as geometry, they will reverse their direction. T is a tag that may be used to start or stop the elevator with the "platform" command; if a non-zero tag is specified, then the platform or elevator will stop upon hitting an obstacle, rather than reversing direction. S is the speed at which the entity moves, in units per second; if S is not specified or 0, it defaults to to 8.

"monster" N [T]

A monster, currently N = 0..4 (see gameplay docs). Monster entities will be spawned when in classic single player mode, and will attack you when you come into view. yaw is taken from the current camera yaw. T is an optional tag number that is assigned to this monster. When the monster dies, the script alias "monster_dead_T" will be invoked.

"respawnpoint"

A respawnpoint for classic SP mode (see "SP Respawning"), when the player dies, they will repsawn at the last one of these they touched, otherwise they start at the playerstart entity.

"particles" type value1 value2 value3 value4

A particle emitter. Particles includes many of the effects as seen for weapons, explosions, and lens flares.
Type 	Values 	Description
0 	radius, height, rgb (0x000..0xFFF) - 0 values are compat with older maps, otherwise radius&height=100 is a 'classic' size 	colored flames with smoke
1 	direction (0..5) 	steam vent
2 	direction (0..5) - color comes from water color 	water fountain
3 	size (0..40), rgb (0x000..0xFFF) 	explosion, i.e. fire ball [*expensive compared to other particles]
4 	direction (0..5), length(0..100), rgb (0x000..0xFFF) 	streak/flare
4 	direction (256+effect), length(0..100), rgb (0x000..0xFFF) 	multiple streak/flare effect
Effect 	Description
0..2 	circlular
3..5 	cylinderical shell
6..11 	conic shell
12..14 	cubic volume
15..20 	planar surface
21 	sphere
note: +32 to reverse direction of streaks
5 	percentage (0..100), rgb (0x000..0xFFF) 	capture meter, i.e. rgb vs black
6 	percentage (0..100), rgb (0x000..0xFFF) 	vs capture meter, i.e. rgb vs bgr
7 	direction, length, rgb - see type 4 	lightning
9 	direction, length, rgb - see type 4 	steam
10 	direction, length, rgb - see type 4 	water
11 	radius, height, rgb - radius&height=100 is a 'classic' size 	flames
12 	radius, height, rgb - radius&height=100 is a 'classic' size 	smoke plume
32 	red (0..255), green (0..255), blue (0..255) 	plain lens flare
33 	red (0..255), green (0..255), blue (0..255) 	lens flare with sparkle center
34 	red (0..255), green (0..255), blue (0..255) 	sun lens flare, i.e. fixed size regardless of distance
35 	red (0..255), green (0..255), blue (0..255) 	sun lens flare with sparkle center