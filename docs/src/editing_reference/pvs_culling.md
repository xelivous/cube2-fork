# PVS Culling

Cube 2 provides a precomputed visibility culling system as described in the technical paper "Conservative Volumetric Visibility with Occluder Fusion" by Schaufler et al (see paper for technical details). Basically, it divides the world into small cube-shaped "view cells" of empty space that the player might possibly occupy, and for each of these view cells calculates what other parts of the octree might be visible from it. Since this is calculated ahead of time, the engine can cheaply look up at runtime whether some part of the octree is possibly visible from the player's current view cell. Once pre-calculated, this PVS (potential visibility set) data is stored within your map and saved along with it, so that it may be reused during gameplay. This data is only valid for a particular map/octree, and if you change your map, you must recalculate it or otherwise expect culling errors. It is recommended you do this only after you are sure you are finished working on your map and ready to release it, as it can take a very long time to compute this data. If you have a multi-core processor or multi-processor system, it can use multiple threads to speed up the pre-calculation (essentially N processors/cores will calculate N times faster).

The number of pre-calculated view cells stored with your map will show up in the edit HUD stats under the "pvs:" stat. It is recommended you keep this number to less than 10,000, or otherwise the amount of storage used for the PVS data in your map can become excessive. For very large SP maps, up to 15,000 view cells is acceptable. The number of view cells is best controlled by use of the "clip" material, or by setting the view cell size (default is 32, equal to a gridpower 5 cube). View cell sizes of 64 or 128 are worth trying if your map still has an excessive number of view cells, but try to use the default view cell size of 32 if it stays reasonable. Note that if you have a map with a lot of open space, there will be a lot of view cells, and so the initial pre-calculation may take a long time. You can use the "clip" material, if necessary, to mark empty space the player can't go into, and the PVS calculation will skip computing view cells for these areas. Filling places the player can't go with solid cubes/sealing the map will similarly reduce the number of possible view cells.

Visibility from a view cell, to some other part of the octree, is determined by looking for large square or block-shaped surfaces and seeing if they block the view from the view cell to each part of the octree. So surfaces like large walls, ceilings, solid buildings, or even mountains and hills, that have large solid cross-sections to them will make the best occluders, and allow the PVS system to cull away large chunks of the octree that are behind them, with respect to the current view cell. Avoid putting holes running entirely through these structures, or this will prevent large cross-section of them from being used as an occluder (since the player could possibly see through them). You can use the "testpvs" command to check how well your occluders are working while building them. If your map is an open arena-style map, then using the PVS system will have little to no effect, since few things are blocking visibility, and it is not worth using the PVS system for such maps.

Note that there is already an occlusion culling system based on hardware occlusion queries, in addition to the PVS system, so the main function of the PVS system is to provide occlusion culling for older 3D hardware that does not support occlusion queries, and also to speed up occlusion queries by reducing the amount of such queries (which can be expensive themselves) even for 3D hardware that supports them. If PVS is used effectively (a map with lots of good occluders), it should always provide some speed-up regardless of whether or not the 3D hardware supports occlusion queries. However, if you are doing open arena-style maps for which there are few good occluders, then it is recommended you skip using the PVS system (as it will just take up memory without providing a speedup) and rely upon the hardware occlusion queries instead.

pvs N

Toggles PVS culling, where N=1 enables it, and N=0 disables it. This is mostly useful for testing the performance effect of the PVS system and should usually be left on.

pvsthreads N

Sets the number of threads (N) that will be used for calculating PVS info with the "genpvs" command. By default, only 1 thread (N=1) is used. If you have N processor or N processor cores, then set this variable to N to make the pre-calculation effectively N times faster. Setting this variable higher than the number of processors/cores will not make it any faster, but setting it lower will not utilize all of them, so try and set it to the exact number.

genpvs [N]

Pre-calculates PVS data for the current version of the map. N is the size of the view cell used for calculation. If N is not specified or 0, then the default view cell size of 32 is used. Try to always use the default view cell size where reasonable.

clearpvs

Clears the PVS data for the map. Use this to clear away stale PVS data if you are editing a map for which PVS data was already pre-calculated to avoid culling errors (i.e. stuff being invisible that should otherwise be visible).

lockpvs N

If N=1, this locks the view cell used by the PVS culling to the current view cell, even if you happen to move outside of it. Everything that was occluded/invisible from that view cell will still be so, even if you move outside of it. This is useful for seeing all the things that are being culled from a current vantage point by the PVS system. If N=0, the view cell is unlocked and PVS will function as normal again.

testpvs [N]

Generates PVS data for only the current view cell you are inside (of size N, or default 32 if not specified) and locks the view cell to it as if "lockpvs 1" were used. This allows you to quickly test the effectiveness of occlusion in your map without generating full PVS data, so that you can more easily optimize your map for PVS before the actual expensive pre-calculation is done. Use "lockpvs 0" to release the lock on the view cell when you are done testing. Note that this will not overwrite any existing PVS data already calculated for the map.

pvsstats

Prints out some useful info about the PVS data stored with the map, such as the number of view cells, the total amount of storage used for all the view cells, and the average amount of storage used for each individual view cell. 