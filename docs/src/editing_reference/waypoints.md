# Waypoints

Cube 2 provides waypoints to aid bot navigation, and without these bots are not able to calculate safe paths from place to place on the map.

Waypoints are entities in a map that tell where it is safe for bots to move to. Each waypoint can link to other waypoints, telling a bot how to get from one waypoint to another. To set up waypoints, first either add a bot (which causes you to drop waypoints as a side-effect) or set the "dropwaypoints" variable to 1. Run around the level through all valid paths, making sure to run through teleporters and touch all items and playerstarts. Waypoints should be dropped at ground level in an even grid across the map, i.e. only jump where it is necesssary to jump. Avoid any movement tricks like weapon jumping that might confuse the AI.

Set the "showwaypoints" variable to 1 so you can see the waypoints you are dropping, and you will see blue lines representing the links between these waypoints. When you are done, use the "savewaypoints" command to save the waypoints for your map; they will be saved to a file named "yourmap.wpt" for a map named "yourmap" in the same directory as your map. It also helps to add some bots before you save and make sure there are no bots stuck at playerstarts without waypoints to guide them.

Note waypoints are not loaded until required, i.e. until a "loadwaypoints" or "addbot" command is issued.

showwaypoints 0/1

Toggles showing of waypoints, where 1 enables it, and 0 disables it. This is mostly useful when laying waypoints so as to see the possible paths and ensure good coverage.

dropwaypoints 0/1

Toggles dropping of waypoints, where 1 enables it, and 0 disables it. By default the player is dropping waypoints whilst playing against bots, this enables bots to "learn" from the player. Note that if enabled, waypoints will be saved automatically once the map is changed, and the variable will then be reset back to 0.

loadwaypoints [filename]

Loads the waypoints for the current map (or specified file).

savewaypoints [filename]

Saves the waypoints for the current map (or specified file), e.g. as "<mapname>.wpt".

clearwaypoints

Removes all waypoints.

delselwaypoints

In editing mode this will remove waypoints within the selection region. 