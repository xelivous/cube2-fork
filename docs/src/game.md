# Game

## Movement, controls & physics

Jumping gains a 30% speedboost while in the air. Moving forwards or backwards without strafing also gains a 30% speedboost. Forward and jumping speedboosts are cumulative.

This has been designed such to give you multiple options to control your speed while navigating a level, you can use strafe for example to slowdown forward motion as you approach a corner to make navigating the corner easier. What this achieves is giving you both speed and precision at the same time, because pure speed (all action being equally fast) just makes you bump into walls. 

## Items and Gameplay

Official game storyline: "You kill stuff. The End." All of the following is assumed to be highly in flux. Once things settled down the document will reflect the changes.

There are initially 7 weapons:

| num | name | damage | reload | dam/sec | technically similar to |
| --: | ---  | -----: | -----: | ------: | ---------------------- |
| 0   | fist | 50 | 0.25s | 200 | quake3 gauntlet |
| 1   | shotgun | 20x10 | 1.4s | 143 | doom2 SSG (wide angle, but less than in doom2) |
| 2   | chaingun | 30 | 0.1s | 300 | quake LG (mild spread at distance) |
| 3   | rocket launcher | 120 | 0.8s | 150 | quake RL (splash damage) |
| 4   | rifle | 100 | 1.5s | 66 | quake2 RG |
| 5   | grenade launcher | 90 | 0.6s | 150 | any quake GL, except faster but less damaging |
| 6   | pistol | 35 | 0.5s | 70 | quake SG, but more precise |

The player has all weapons available when he spawns, but no ammo for them (except 40 pistol ammo), 100 health, and no armour. Health and ammo respawn time depends on number of players in the game, the numbers given are for 1-2, 3-4, and 5+ player games respectively.

Ammo that can be picked up:

| weapon | name | amount | max | respawn times |
| ------ | ---- | -----: | --: | ------------ |
| shotgun | shells | 10 | 30 |  16s/12s/8s |
| rocket launcher | rockets | 5 | 15 | 16s/12s/8s |
| chaingun | bullets | 20 | 60 | 16s/12s/8s |
| rifle | rounds | 5 | 15 | 16s/12s/8s |
| grenade launcher | grenades | 10 | 30 | 16s/12s/8s |
| pistol | ammo | 30 | 120 | 16s/12s/8s |

Other Items:

| name | effect | respawn |
| ---- | ------ | ------- |
| health | adds 25 to a max of MAXHEALTH (initially 100) | 20s/15s/10s |
| health boost | add 100 to health and 50 to max health | --- |
| light armour | 100 armour, absorbs 50% of damage | 20s |
| heavy armour | 200 armour, absorbs 75% of damage | 30s |
| quad powerup | 4x damage for 20 seconds | 70s |


