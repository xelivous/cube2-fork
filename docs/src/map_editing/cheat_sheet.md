# Cheat Sheet

| key | function |
| --- | -------- |
| Mouse Left Button | Select Faces |
| Mouse Middle Button | Select Corners |
| Mouse Right Button | Extend selection; Reorient section direction (requires initial selection) |
| Mouse Wheel | Pull cubes into existence and push them out of existence |
| Spacebar | Deselect |
| F+Mouse Wheel | Push and pull all 4 corners at once (in the case of no corner selection) |
| R+Mouse Wheel | Rotate relative to the white box |
| Y+Mouse Wheel | Quick texture change |
| G+Mouse Wheel | change grid size |
| U | undo one step |
| X | Mirror relative to the side of the white box |
| F2 | Texture menu |
| Keypad Enter | Selection entities within selection |
| /savemap | mapname to save your map |