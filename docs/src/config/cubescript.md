# CubeScript

Cube's console language is similar to console languages of other games (e.g. Quake), but is a bit more powerful in that it is a full programming language.

What is similar to quake is the basic command structure: commands consist of the command itself, followed by any number of arguments seperated by whitespace. you can use `""` to quote strings with whitespace in them (such as the actions in bind/alias), and whereever a command is required you can also use ; to sequence multiple commands in one.

What is new compared to quake is that you can evaluate aliases and expressions. You can substitute the value of an alias as an argument by prefixing it with a `$` sign, (example: echo The current value of x is $x). You can even substitute the values of console variables this way, i.e `$fov` gives the current fov. Some aliases are set automatically, for example `$arg1` to `$argN` are set (as if by the "push" command) if you supply arguments when you execute an alias, and are popped when the alias finishes executing.

There are two alternatives to `""` for quoting a string: `()` and `[]`. They work in the same way as `""`, with the difference that they can be nested infinitely, and that they may contain linefeeds (useful for larger scripts). `()` is different from `[]` in that it evaluates the commands contained in it _before_ it evaluates the surrounding command, and substitutes the results. `()` bracketed strings are called expressions, while `[]` bracketed strings may be thought of as blocks.

An alternative to `$x` is `@x`, which uses an alias as a macro. The difference is that `@x` can be substituted inside `[]` forms before they have ever been evaluated (at parse time), which makes them useful for composing strings or creating code on the fly. The `@x` form will be substituted using the value of x at the time the enclosing `[]` is evaluated. You can add more `@` prefixes to move up more levels of `[]`s, so `@@x` will move up two levels of `[]`s and so on. Example: `x = 0; if $cond [x = 1; [x = 2; echo @@x]]` will echo `0`, since it uses the value of `x` at two levels up.

The form `@(body)` is similar to `@x`, except that `body` contains commands executed at parse time. The result value after `body` executes is substituted in for `@(body)`. You may use multiple `@` prefixes as with the `@x` form. Example: `@(result "Hello, World!")` will substitute itself with `Hello, World!`

`@[body]` is also similar to `@(body)`, except that it does not execute its contents and instead expands macros to use as the alias name for the final macro substitution.

The following commands are available for programming:

### `+ A B` & `- A B` & `* A B` & `div A B` & `mod A B`
(add, substract, multiply, divide, modulo): these all work like the integer operators from other languages (example: echo x squared is (* $x $x)).

### `= A B` & `< A B` & `> A B` & `strcmp A B`
(equals, lessthan, greaterthan, stringcompare): comparison operators that return 1 for true and 0 for false.

### `! A` & `&& A B` & `|| A B` & `^ A B`
(not, and, or, xor): boolean operators. 0 is false, everything else is true. The AND and OR operators are implemented to shortcut.

### `strstr S N`
Searches the string S for the substring N and returns its starting position. This function is case sensitive, when N is not found it returns -1. (example: echo (strstr "for example this sentence" "this" ) - would return 14 ).

### `substr S A B`
Returns a part from the string S starting from A for B chars (example: echo (substr "grab this part here" 11 4 ) - would return "here" ).

### `strreplace S A B`
This searches the string S for substring A and replaces it with another substring B. (example: echo ( strreplace "this is serious" "serious" "fun" ) - would return "this is fun" ).

### `strlen S`
Returns the length of the string S (example: echo (strlen "long sentence") - would return 13 ).

### `min A B`
This results the lower number of the 2 Strings A and B

### `max A B`
This results the higher number of the 2 Strings A and B

### `rnd N`
Grabs a random number from 0 to N, and results it.

### `if cond true false`
Executes the true or false part depending on wether cond is "0" or something else (example: if (< $x 10) [ echo "x is" $x ] [ echo "x is too big" ]).

### `loop I N body`
Evaluates body N times, and sets the alias I from 0 to N-1 for every iteration (example: loop i 10 [ echo $i ]).

### `while cond body`
Evaluates body while cond evaluates to true. Note that cond here has to have [], otherwise it would only be evaluated once (example: i = 0; while [ (< $i 10) ] [ echo $i; i = (+ $i 1) ]).

### `concat S...`
Concatenates all the arguments and returns the result

### `concatword S...`
Same as concat but without spaces between the elements.

### `format F V1..Vn`
Substitutes the V values into the format string F and returns the result. The format strings %1 through %9 are substituted with V1 through V9, respectively, and may be used multiple times. %% will generate a single % character (example: format "%1 bottles of %2 on the %3, %1 bottles of %2!" 99 beer wall).

### `at S N`
Grabs the Nth word out of string S and returns the result

### `listlen L`
Returns the number of items in the list L

### `listclients N`
Gives a list of all playernumbers if N is 1 if N is 0 it lists all players except you.

### `onrelease A`
Only executes A if the command is executed on the release of a key/button (must be in an action in a bind or an alias in a bind).

### `result V`
Normally the result of a [] block is the result of the last command in the block. If you want the result to be a particular variable or value, you can use e.g. "result $i" etc. 

## Variables that are only really useful when used as value:

### `$editing`
This is true when in edit mode.

### `getalias V`
Returns the alias from the variable V.

### `getbind B`
Returns the game bind B

### `geteditbind B`
Return the edit bind B

### `getspecbind B`
Returns spectator bind B

### `getclientnum N` 
Returns the client number of the player N.

### `getclientname N`
Returns the client name of the player N.

### `getclientteam N`
Returns the client team of the player N.

### `getname` & `getteam` & `getweapon` & `getfps`
Tells your own Name Team Weapon or FPS

### `gettex`
Adds the current selected texture to the front of your texture list in Y+scroll Needs Allfaces set to 0 so it works.

### `gettexname`
Results the current Texture name

### `isconnected N`
If the Player N is online this is 1 (true)

### `isspectator N`
If the Player N is spectator this is 1 (true) 