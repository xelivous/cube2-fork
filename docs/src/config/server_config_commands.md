# Server Configuration Commands

The following commands may be placed in the file "server-init.cfg" in the root Sauerbraten directory to configure a dedicated server, or they may be used inside a running client to config ure a listen server:

### `startlistenserver [MASTER]`
Starts a listen server from within a running game client. If MASTER=1, then the server will report to the master server, otherwise if MASTER=0 or not supplied the server will not report to the master server. The various server configuration commands can be used before this command to setup properties of the listen server.

### `stoplistenserver`
Stops a listen server running from within a game client.

### `serverip S`
Sets the IP the server should bind/listen to S. This is only useful if your server is running on a host with multiple interfaces.

### `serverport N`
Sets the port the server should bind/listen to N. By default, server's listen on port 8785.

### `maxclients N`
Sets the maximum number of clients that can connect to the server to N.

### `serverbotlimit N`
Sets the maximum number of bots a master can add to N. Admins are unaffected by this limit.

### `publicserver B`
Toggles whether a server is considered a "public" server when B=1: can only gain master by "auth" or admin and doesn't allow locked/private mastermodes. B=0 allows "setmaster 1" and locked/private mastermodes (for coop-editing and such).

### `serverdesc S`
Sets the description shown for the server in the server browser to S.

### `serverpass S`
Sets the password required to connect to the server to S. This option is only useful if you don't want anyone connecting to your server.

### `adminpass S`
Sets the password to gain admin access to a server to S, as well as the password that may be used to override the private mastermode when connecting.

### `servermotd S`
Sets the "message of the day", a message shown to users when they connect to the server, to S.

### `updatemaster B`
Toggles whether or not the server should report to the masterserver. B=1 enables (default) and B=0 disables.

### `mastername S`
Sets the IP of the master server the server reports to. This defaults to "sauerbraten.org" and generally should not be changed.

### `restrictdemos B`
Toggles whether admin is required to start recording a demo. B=1 enables (default) and B=0 disables.

### `maxdemos N`
The maximum number of demos the server will store (default: 5).

### `maxdemosize N`
The maximum size a demo is allowed to grow to in megabytes (default: 16).

### `maxdemosize N`
Toggles whether admin is required to start recording a demo. B=1 enables (default) and B=0 disables.

### `ctftkpenalty B`
Toggles whether teamkilling the flag runner in CTF modes should disallow the teamkiller from stealing the flag (default: 1).
