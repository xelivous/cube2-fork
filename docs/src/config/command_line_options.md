# Command line options

### `-d`
This starts as a dedicated server. The default is a non-dedicated server with only a single client in graphical mode. Dedicated servers run in the shell only (no graphics), with increased priority yet use very little cpu time and memory, so you can run one in the background, or at the same time with a client if you want to host a game (which is usually better than using a listen server). Servers use the ports 28785 (UDP) and 28786 (UDP).

### `-d[N]`
If N=1, starts a listen server which allows it to simultaneously function as both a client and server. Note that a listen server is limited by your in-game frame rate, so you should use a dedicated server if your graphics card is slow or you have enabled frame rate limiting options such as "vsync". If N=2, it starts a dedicated server as for the "-d" command-line option.

### `-w[N]`
Sets the screen resolution width to N (default: 640).

### `-h[N]`
Sets the screen resolution height to N (default: 480).

### `-z[N]`
Sets the z-buffer precision to N bits. This should be at least 24 or you may experience depth problems.

### `-a[N]`
Sets FSAA (Full Scene AntiAliasing) to N samples, i.e. -a4 is 4xFSAA

### `-t`
Sets Sauerbaten to run windowed.

### `-qS`
Sets S to your home directory. If set, the engine will look for files in your home directory as well as the normal installation directory. However, all files will be written to your home directory, instead of the normal installation directory.

### `-kS`
Adds the mod directory S to the list of directories the engine will search in for files. Directories will be searched in the order listed on the command-line, and if not found, the engine looks in the installation directory.

### `-g[S]`
Sets the log file to S. All console output is written to the log file.

### `-l[S]`
Loads map S on startup.

### `-x[S]`
Executes the script commands S on startup, note that this is done before the map loads, so if you wish them to be executed later, you should enclose them in a sleep statement. Example: -x"sleep 1000 [ connect localhost ]"

### `-u[N]`
Sets the server upstream bandwidth to N bytes per second. Only set this parameter if you know what you are doing, specifying a wrong value is worse than not specifying it at all.

### `-n[N]`
Sets the server description, for people pinging the server. usually does not need to be set if you have a descriptive domain name already, but if you set it, keep it short as it may otherwise be truncated (example: -n"Bobs Instagib Server").

### `-c[N]`
Sets the max number of clients to N. The default is 4. If you want to set it higher, be aware that bandwidth usage almost doubles with each extra client, so only do this if the server runs on a serious pipe (not your home DSL or Cable connection).

### `-i[S]`
Sets the server ip to S. This option is only useful for people running on machines with multiple network interfaces.

### `-j[N]`
Sets the server port to N. This option is only useful for people running on machines with multiple network interfaces. Note that both UDP ports N (game traffic) and N+1 (pings) must be available for this to work. If not specified, the default ports used are 28785 and 28786. Regardless of what ports are set, port 28784 must be available for pinging servers over a LAN to work.

### `-m[S]`
Sets the master server to use for either server (registering) and client (updating) to S. (default: sauerbraten.org).

### `-p[S]`
Sets the server's administrative password to S.

### `-y[S]`
Locks the server so that password S must be provided to connect to it.

### `-o[N]`
Sets the openness of the server to N. N=1 disables mastermodes 2 (locked) and 3 (private), while N=0 (default) enables all mastermodes. 