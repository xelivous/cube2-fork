# GUI Commands

### `gui2d B`
Sets whether menus should be shown in 2D, or 3D if disabled (default 0).

### `menudistance D`
Sets the distance at which the menu is created in front of the player (default 40).

### `menuautoclose D`
Sets the distance at which the menu is automatically closed when the player moves away from it (default 120).

### `guiautotab H`
Sets the height of the gui before tabs are automatically generated.

### `cleargui`
Hides the menu.

### `showgui N`
Displays the menu with name N previously defined, and allows the user to pick and manipulate items with the cursor. Pressing ESC will cancel the menu.

### `newgui S`
Creates a new menu with name S. All the following 'gui' menuitem commands will apply to this menu. See "data/menus.cfg" for defaults.

### `guilist '[' ... ']'`
Defines a menu item that is a group of menu items. The layout of each nested group will alternate - the top level (newgui) is laid out vertically, the first guilist will be laid out horizontally, the second guilist vertically, etc. This allows tables and lists to be created. The layout direction also influences the display of items such as sliders, bars, progress bars, and struts.

### `guispring [W]`
A list containing springs will fill its parent list along its layout direction by dividing any needed space among the springs. The spring is weighted by a positive integer W, otherwise defaults to W=1.

### `guititle S`
Creates a menu item with horizontally centered title S.

### `guibutton [S] A [I]`
Creates a menu button with the name S and icon I, which will execute A when selected. If you only specify one parameter, then it will be used for both. The icon used will be I if supplied, otherwise a blue-button if A contains showgui, otherwise it is a green button. Clicking a button will close the menu.

### `guiimage P A S [O]`
Creates a menu image tile from the path P, which will execute A when selected. Clicking the image will close the menu. Uses image scale S and O=1 if an overlay is required.

### `guitext S`
Creates a menu item consisting of the text line S and an information icon.

### `guitextbox S W [H] [C]`
Creates a text box showing the text S, with width W, height H, and color C. The default is a single line of white text. Note that the height will increase automatically so as to enclose all the text.

### `guibar`
Creates a vertical/horizontal menu bar. The orientation depends on the layout direction.

### `guislider V [MIN MAX A]`
Creates a vertical/horizontal menu slider that is bound to a variable V. The min/max bounds of the slider will be as defined by the data model of the variable unless explicitly specified. An action A can also be specified for every slider change. The orientation of the slider depends on the layout direction.

### `guicheckbox N V [ON OFF A]`
Creates a menu checkbox with a label N that is bound to a variable V. The default value are 0/1 unless supplied by ON/OFF. An action A can also be specified for every checkbox change.

### `guiradio N V VAL [A]`
Creates a menu radio button with a label N that is bound to a variable V. The radio button is only ticked when the variable has value VAL, and presing the button will update the variable to the value VAL. An action A can also be specified for every radio button change.

### `guitab S`
Creates a new menu tab with title S. Note: has no effect if used within a guilist.

### `guifield A N [O [U]]`
Creates a field which accepts editable input, the value of which is taken from and updated to (upon change) the alias A, and can have at most N characters. If specified, O is executed when the user presses enter. If U is also specified, it will execute it every every frame (unless it is currently being edited) to allow updating the field. Note that all fields that share the same A alias, also share the same edit info, if the user moves outside the field the changes are not committed, if they select another field, changes will be lost. 
