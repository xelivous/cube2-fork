# Config

Take some time to read the rest of this document, as it describes options and commands that may be crucial to a successful experience with Cube 2: Sauerbraten.

Binding keys, as well as performance and gameplay related settings are documented below, these commands can be used in both the console and configuration scripts. In addition, you can also press "ESC" to go into the menu, which will provide you with an interface for navigating some (not all) of the commands. 

## Running

* For Windows: Install and then run sauerbraten.bat or select the entry from the Start menu.
* For Linux: Extract and then run ./sauerbraten_unix inside the sauerbraten directory. Needs a decent and compliant OpenGL implementation.

The game and engine derives its simplicity from some rather brute force rendering methods, and as such it needs a beefy machine to get good visual quality. It runs best with vertical sync (vsync) set to OFF and at high refresh rates (otherwise, you may get excessive LOD/FPS variance). 